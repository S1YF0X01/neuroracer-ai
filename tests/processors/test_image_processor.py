from __future__ import print_function
import os
import unittest

import cv2
import numpy as np

from neuroracer_ai import ImageProcessingParameters, ImageProcessor


class TestImageProcessor(unittest.TestCase):

    def setUp(self):
        self.test_image = self.read_image("tiny_test_image.png")

    @staticmethod
    def read_image(img_name, flags=-1):
        full_path = os.path.join(os.path.dirname(__file__), "test_images", img_name)
        image = cv2.imread(full_path, flags)

        if image is None:
            raise IOError("{} couldn't be loaded!".format(full_path))

        return image

    @staticmethod
    def create_image_processor(**kwargs):
        image_processor_params = ImageProcessingParameters(**kwargs)
        return ImageProcessor(image_processor_params)

    def cropping_test(self, img_name, **kwargs):
        crop_parameters = ImageProcessingParameters.CropParameters(**kwargs)
        image_processor = self.create_image_processor(crop_parameters=crop_parameters)

        result = image_processor.process(self.test_image)
        self.assertTrue(
            np.array_equal(
                result,
                self.read_image(img_name)))

    def test_relative_left_cropping(self):
        self.cropping_test(
            "tiny_test_image_cropped_l40.png",
            left=0.2)

    def test_relative_right_cropping(self):
        self.cropping_test(
            "tiny_test_image_cropped_r40.png",
            right=0.2)

    def test_relative_top_cropping(self):
        self.cropping_test(
            "tiny_test_image_cropped_t40.png",
            top=0.2)

    def test_relative_bottom_cropping(self):
        self.cropping_test(
            "tiny_test_image_cropped_b40.png",
            bottom=0.2)

    def test_relative_full_cropping(self):
        self.cropping_test(
            "tiny_test_image_cropped_all20.png",
            left=0.1, right=0.1, top=0.1, bottom=0.1)

    def test_absolute_left_cropping(self):
        self.cropping_test(
            "tiny_test_image_cropped_l40.png",
            left=40, relative=False)

    def test_absolute_right_cropping(self):
        self.cropping_test(
            "tiny_test_image_cropped_r40.png",
            right=40, relative=False)

    def test_absolute_top_cropping(self):
        self.cropping_test(
            "tiny_test_image_cropped_t40.png",
            top=40, relative=False)

    def test_absolute_bottom_cropping(self):
        self.cropping_test(
            "tiny_test_image_cropped_b40.png",
            bottom=40, relative=False)

    def test_absolute_full_cropping(self):
        self.cropping_test(
            "tiny_test_image_cropped_all20.png",
            left=20, right=20, top=20, bottom=20, relative=False)

    def flip_test(self, img_name, flip_option):
        image_processor = self.create_image_processor(flip_option=flip_option)

        result = image_processor.process(self.test_image)
        self.assertTrue(
            np.array_equal(
                result,
                self.read_image(img_name)))

    def test_vertical_flip(self):
        self.flip_test(
            "tiny_test_image_flipped_v.png",
            ImageProcessingParameters.Flip.vertical)

    def test_horizontal_flip(self):
        self.flip_test(
            "tiny_test_image_flipped_h.png",
            ImageProcessingParameters.Flip.horizontal)

    def test_full_flip(self):
        self.flip_test(
            "tiny_test_image_flipped_both.png",
            ImageProcessingParameters.Flip.horizontal_and_vertical)

    def test_change_color_space(self):
        image_processor = self.create_image_processor(opencv_color_space_conv_func=cv2.COLOR_BGR2GRAY)
        result = image_processor.process(self.test_image)

        self.assertTrue(
            np.array_equal(
                result,
                self.read_image("tiny_test_image_gray.png", 0)  # import as gray scale
            )
        )

    def resize_test(self, image, **kwargs):
        resize_parameters = ImageProcessingParameters.ResizeParameters(**kwargs)
        image_processor = self.create_image_processor(resize_parameters=resize_parameters)
        result = image_processor.process(self.read_image("tiny_test_image.png"))

        self.assertTrue(
            np.array_equal(
                result,
                self.read_image(image)
            )
        )

    def test_relative_resize(self):
        self.resize_test("tiny_test_image_resize_increase.png", x_scaling=2, y_scaling=2)

    def test_relative_resize_decrease(self):
        self.resize_test("tiny_test_image_resize_decrease.png", x_scaling=0.25, y_scaling=0.25, relative_scaling=False)

    def test_absolute_resize_increase(self):
        self.resize_test("tiny_test_image_resize_increase.png", x_scaling=400, y_scaling=400, relative_scaling=False)

    def test_absolute_resize_decrease(self):
        self.resize_test("tiny_test_image_resize_decrease.png", x_scaling=50, y_scaling=50, relative_scaling=False)

    def test_normalization(self):
        image_processor = self.create_image_processor(normalize=True)
        normalized_img = cv2.normalize(self.test_image.astype(np.float32), None, 0, 1, cv2.NORM_MINMAX)

        result = image_processor.process(self.test_image)

        self.assertTrue(np.array_equal(result, normalized_img))


if __name__ == '__main__':
    unittest.main()
