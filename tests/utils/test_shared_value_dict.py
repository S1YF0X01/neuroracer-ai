import unittest

from neuroracer_ai.utils.shared_value_dict import SharedValueDict


class TestSharedValueDict(unittest.TestCase):

    def setUp(self):
        self.test_dict = SharedValueDict({"one": 1, "two": 2, "three": 3, "another_one": 1})
        self.empty_dict = SharedValueDict()
        self.overwritable_dict = SharedValueDict()

    def test_len(self):
        self.assertEqual(len(self.test_dict), 4)
        self.assertEqual(len(self.empty_dict), 0)

    def test_get_item(self):
        self.assertEqual(self.test_dict["one"], 1)
        self.assertEqual(self.test_dict["two"], 2)
        self.assertEqual(self.test_dict["three"], 3)
        self.assertEqual(self.test_dict["another_one"], 1)

        with self.assertRaises(KeyError):
            self.test_dict["unknown_key"]
            self.empty_dict["unknown_key"]

    def test_set_item(self):
        self.overwritable_dict["new_key"] = 4
        self.assertEqual(self.overwritable_dict["new_key"], 4)

    def test_iter(self):
        keys = {"one", "two", "three", "another_one"}
        test_keys = set()
        for a in self.test_dict:
            test_keys.add(a)

        self.assertEqual(keys, test_keys)

    def test_keys(self):
        keys = ["one", "two", "three", "another_one"]
        self.assertEqual(set(self.test_dict.keys()), set(keys))

    def test_values(self):
        values = [1, 2, 3]
        self.assertEqual(set(self.test_dict.values()), set(values))

    def test_items(self):
        items = [("one", 1), ("two", 2), ("three", 3), ("another_one", 1)]
        self.assertEqual(set(self.test_dict.items()), set(items))

    def test_has_key(self):
        self.assertTrue(self.test_dict.has_key("one"))
        self.assertTrue(self.test_dict.has_key("two"))
        self.assertTrue(self.test_dict.has_key("three"))
        self.assertTrue(self.test_dict.has_key("another_one"))
        self.assertFalse(self.test_dict.has_key("unknown_key"))

    def test_get(self):
        self.assertEqual(self.test_dict.get("one"), 1)
        self.assertEqual(self.test_dict.get("two"), 2)
        self.assertEqual(self.test_dict.get("three"), 3)
        self.assertEqual(self.test_dict.get("another_one"), 1)

        with self.assertRaises(KeyError):
            self.test_dict.get("unknown_key")
            self.empty_dict.get("unknown_key")

    def test_clear(self):
        tmp_dict = SharedValueDict({"one": 1, "two": 2, "three": 3, "another_one": 1})
        self.assertEqual(len(tmp_dict), 4)

        tmp_dict.clear()

        self.assertEqual(len(tmp_dict), 0)

    def test_iter_keys(self):
        keys = set()
        for k in self.test_dict.iterkeys():
            keys.add(k)

        test_keys = {"one", "two", "three", "another_one"}
        self.assertEqual(keys, test_keys)

    def test_itervalues(self):
        values = set()
        for k in self.test_dict.itervalues():
            values.add(k)

        test_values = {1, 2, 3}
        self.assertEqual(values, test_values)

    def test_iteritems(self):
        items = set()
        for i in self.test_dict.iteritems():
            items.add(i)

        test_items = {("one", 1), ("two", 2), ("three", 3), ("another_one", 1)}
        self.assertEqual(test_items, items)

    def test_copy(self):
        dict_copy = self.test_dict.copy()
        self.assertTrue(dict_copy == self.test_dict)
        self.assertFalse(dict_copy is self.test_dict)


if __name__ == '__main__':
    unittest.main()
