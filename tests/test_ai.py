import os
import time
import unittest

import cv2
import numpy as np
import keras.models as km
import keras.initializers as ki
import keras.layers as kl

from neuroracer_ai.models import AbstractModel
from neuroracer_ai import AI, TrainParameters


class TestAI(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        base_dir = os.path.dirname(__file__)
        cls.model_dir = os.path.join(base_dir, "models")
        if not os.path.exists(cls.model_dir):
            os.mkdir(cls.model_dir)

        cls.image = cv2.imread(os.path.join(base_dir, "test_image.png"))  # type: np.ndarray
        cls.image_shape = cls.image.shape

        # uncomment only if you want to renew the test data after a change. you
        # have to validate the correctness of the data manually after renewal
        # TestAi.renew_test_models(cls.model_dir, cls.image)

    @staticmethod
    def renew_test_models(model_dir, image):
        ai = AI.create("test_model", model_dir, TestAi.architecture, {"image_shape": image.shape})

        data = np.expand_dims(image, axis=0)
        label = np.array([[0.3, 0.7]])

        ai.train(
            TrainParameters(
                train_data=data,
                train_data_label=label,
                validation_data=data,
                validation_data_label=label,
                use_checkpoints=False,
                batch_size=10,
                epochs=3))

        ai.save(model_dir)
        raise SystemExit("renewed the models! uncomment the creation now.")

    @staticmethod
    def architecture(image_shape):
        input = kl.Input(shape=image_shape, name="observation_input")

        conv = kl.Conv2D(activation="relu", filters=1, kernel_size=(5, 5), padding="valid", name="conv",
                         kernel_initializer=ki.VarianceScaling(mode="fan_in", distribution="uniform"))(input)

        observation_flattened = kl.Flatten(name="flattened_observation")(conv)

        output = kl.Dense(units=2, activation="relu", name="prediction",
                          kernel_initializer="glorot_uniform")(observation_flattened)

        model = km.Model(inputs=input, outputs=output)
        model.compile(loss="mse", optimizer="adam", metrics=["mse"])

        return model

    def test_create(self):
        ai = AI.create("create_test", self.model_dir, self.architecture, {"image_shape": self.image_shape})

        self.assertNotEqual(ai, None)
        self.assertNotEqual(ai._model, None)
        self.assertTrue(isinstance(ai._model, AbstractModel))
        self.assertNotEqual(ai._model._internal_model, None)

    def test_load(self):
        ai = AI.load("test_model", self.model_dir)

        self.assertNotEqual(ai, None)
        self.assertNotEqual(ai._model, None)
        self.assertTrue(isinstance(ai._model, AbstractModel))
        self.assertNotEqual(ai._model._internal_model, None)

    def test_create_and_direct_load(self):
        model_name = "create_and_load_test"

        ai = AI.create(model_name, self.model_dir, self.architecture, {"image_shape": self.image_shape})
        ai.save(self.model_dir, )
        ai = AI.load(model_name, self.model_dir)

        self.assertNotEqual(ai, None)
        self.assertNotEqual(ai._model, None)
        self.assertTrue(isinstance(ai._model, AbstractModel))
        self.assertNotEqual(ai._model._internal_model, None)

    def test_predict(self):
        ai = AI.load("test_model", self.model_dir)
        data = np.expand_dims(self.image, axis=0)
        result = ai.predict(data)

        self.assertTrue(result is not None)

    def test_save(self):
        ai = AI.create("save_model", self.model_dir, self.architecture, {"image_shape": self.image_shape})
        start_time = time.time()
        model_path = os.path.join(self.model_dir, "save_model.hdf5")
        ai_config_path = os.path.join(self.model_dir, "save_model.nrai")

        time.sleep(0.05)  # to prevent small differences between the start and the write time of the config

        ai.save(self.model_dir)

        self.assertTrue(os.path.exists(model_path))
        self.assertTrue(os.path.exists(ai_config_path))

        self.assertGreaterEqual(os.path.getmtime(model_path), start_time)
        self.assertGreaterEqual(os.path.getmtime(ai_config_path), start_time)

        reloaded_ai = AI.load("save_model", self.model_dir)
        self.assertNotEqual(reloaded_ai, None)

    def test_train(self):
        ai = AI.create("train_test", self.model_dir, self.architecture, {"image_shape": self.image_shape})

        batch_size = 3
        epochs = 2

        data = np.expand_dims(self.image, axis=0)
        label = np.array([[0.3, 0.7]])

        history = ai.train(
            TrainParameters(
                train_data=data,
                train_data_label=label,
                validation_data=data,
                validation_data_label=label,
                use_checkpoints=False,
                batch_size=batch_size,
                epochs=epochs))

        self.assertNotEqual(history, None)
        self.assertEqual(history.params["epochs"], epochs)
        self.assertEqual(history.params["batch_size"], batch_size)
        self.assertEqual(len(history.epoch), epochs)

    def test_visualize_activation(self):
        ai = AI.load("test_model", self.model_dir)

        result = ai.visualize_activation("prediction")

        self.assertTrue(result is not None)

    def test_visualize_saliency(self):
        ai = AI.load("test_model", self.model_dir)

        result = ai.visualize_saliency("prediction")

        self.assertTrue(result is not None)
