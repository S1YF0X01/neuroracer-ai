# neuroracer-ai
This is a helper library for the Neurorace project. It was written to improve the usability and allows developers to interact with a simple api instead of different complex backend functions.

It is mainly intended to be used in combination with Neurorace Project. 

At the moment it is only available through this repository. In near future, it will be available by pip (pypi).

## On this page

- [Installation](#installation)
- [Usage](#usage)
  - [Examples](#examples)
     - [Instantiating the api and creating a new model](#instantiating-the-api-and-creating-a-new-model)
     - [Loading a model](#loading-a-model)
     - [Training a model](#training-a-model)
     - [Manually saving a model](#manually-saving-a-model)
  - [CLI tools](#cli-tools)
  - [Integration into the Neurorace Racecar](#integration-into-the-neurorace-racecar)

## Installation
To install the library for normal usage, clone it and install the cloned folder with `pip install`.

For extended usage use `pip install -e` instead. The command will install the package in editable mode.

## Usage
The following will explain the provided functions ans their usage.

### Examples
#### Instantiating the api and creating a new model
```python
import os

from neuroracer_ai import AI, KerasArchitectures

model_name = "example"
model_dir = os.path.join(os.path.dirname(__file__), "models")

ai = AI.create(name=model_name, # name of the ai model 
               model_dir=model_dir, # directory at which the model gets saved 
               architecture=KerasArchitectures.nvidia, # nn architecture which is used
               architecture_params={"image_shape": (256, 256, 3)}) # parameters for the used nn architecture
        
# do things with your new ai instance
```

#### Loading a model
```python
import os

from neuroracer_ai import AI, KerasArchitectures

model_name = "example"
model_dir = os.path.join(os.path.dirname(__file__), "models")

ai = AI.load(name=model_name,
             model_dir=model_dir)
        
# do things with your new ai instance
```

#### Training a model
todo: finish this part
```python
# ai variable should be an already instantiated api instance 
ai # type: neuroracer_ai.AI

from neuroracer_ai import TrainParameters 

train_params = TrainParameters(train_data=None,
                               train_data_label=None, 
                               validation_data=None, 
                               validation_data_label=None, 
                               checkpoint_dir=None, 
                               use_checkpoints=None, 
                               batch_size=None, 
                               epochs=None)

ai.train(train_parameters=train_params)
```

#### Manually saving a model
```python
# ai variable should be an already instantiated api instance 
ai # type: neuroracer_ai.AI

model_dir = os.path.join(os.path.dirname(__file__), "models")

ai.save(directory=model_dir,
        overwrite=True)
```

### CLI tools
Besides the api itself, the package contains a helpful cli tools:

- `nrai-train` 

  See [the nrai-train usage](./neuroracer_ai/nrai-train.md)

- `nrai-converter`

  See [the nrai-convert usage](./neuroracer_ai/nrai-convert.md)
  
- `nrai-augment`

  See [the nrai-augment usage](./neuroracer_ai/nrai-augment.md)

### Integration into the Neurorace Racecar 
todo: write something
