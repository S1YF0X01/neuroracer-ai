#!/usr/bin/env python

from setuptools import setup, find_packages
from codecs import open
from os import path
from platform import uname

here = path.abspath(path.dirname(__file__))

with open(path.join(here, "README.md"), encoding="utf-8") as file_handle:
    long_description = file_handle.read()

# common dependencies
install_req = [
    "ruamel.yaml",  # todo: backtrace the oldest usable version
    "pymp-pypi>=0.4.0",
    "h5py>=2.6.0",
    "numpy>=1.11.1",
    "scikit-image>=0.12.3",
    "scikit-learn>=0.17.1",
    "scipy>=0.18.0",
    "keras>=2.0.0",
    "pydot>=1.1.0",
    "tqdm",  # TODO specify >= version
    "matplotlib",  # TODO specify >= version
    "pymp-pypi",  # TODO specify >= version
    "flufl.enum",  # TODO specify >= version
    "keras-vis"  # TODO specify >= version
]

# only add OpenCV when not installing on Jetson
if "tegra-ubuntu" != uname()[1]:
    install_req.append("opencv-python>=3.2.0")

setup(
    name="neuroracer-ai",
    version="0.1.0",
    description="AI Library for the NeuroRace Project",
    long_description=long_description,
    long_description_content_type='text/markdown',
    url="https://gitlab.com/NeuroRace",
    # author="",
    # author_email="",
    classifiers=[
        "Development Status :: 1 - BETA",
        "Intended Audience 2:: Developers",
        "Topic :: Utilities",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 2.7"
    ],
    keywords="ai ros autonomous",
    packages=find_packages(exclude=["docs, tests"]),
    install_requires=install_req,
    test_suite="tests",
    project_urls={
        "Bug Reports": "https://gitlab.com/NeuroRace/neuroracer-ai/issues",
        "Source": "https://gitlab.com/NeuroRace/neuroracer-ai",
    },
    entry_points={"console_scripts": [
        "nrai-train = neuroracer_ai.cli.train:main",
        "nrai-convert = neuroracer_ai.cli.convert:main",
        "nrai-augment = neuroracer_ai.cli.augment:main",
        "nrai-debug = neuroracer_ai.cli.debug:main"
    ]}
)
