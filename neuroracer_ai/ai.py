"""
This module is everything a user has to import to use the library. The main api
class is AI. It contains every available call. The other classes are all helper
class to simplify the configuration.
"""

import numpy

import neuroracer_ai.utils.file_handler as fh
from models import Debuggable
from neuroracer_ai.suites import EmptyProcessorSuite
from neuroracer_ai.models import KerasModel


class TrainParameters:
    """
    Parameters which are used for the training.
    """

    def __init__(self, train_data_generator, validation_data_generator, checkpoint_dir=None,
                 use_checkpoints=True, batch_size=120, epochs=10):
        """
        :param train_data: train x data
        :type train_data: numpy.ndarray
        :param train_data_label: train y data
        :type train_data_label: numpy.ndarray
        :param validation_data: validation x data
        :type validation_data: numpy.ndarray
        :param validation_data_label: validation y data
        :type validation_data_label: numpy.ndarray
        :param checkpoint_dir: Path in which checkpoints are written
        :type checkpoint_dir: str
        :param use_checkpoints: Enables the saving of checkpoints. Defaults to True
        :type use_checkpoints: bool
        :param batch_size: Size of the batches which are used. Defaults to 120.
        :type batch_size: int
        :param epochs: Amount of epochs to train. Defaults to 10.
        :type epochs: int
        """

        self.train_data_generator = train_data_generator
        self.validation_data_generator = validation_data_generator
        self.checkpoint_dir = checkpoint_dir
        self.use_checkpoints = use_checkpoints
        self.batch_size = batch_size
        self.epochs = epochs


class AI:
    def __init__(self, model, name, model_dir, processor_suite=EmptyProcessorSuite()):
        """
        Constructor of the AI class. Should never be called outside of the ai
        class. It should be considered private!

        :param model: model instance that is used for prediction and training
        :type model: neuroracer_ai.models.AbstractModel
        :param name: name of which the ai/ model uses to save.
        :type name: str
        :param model_dir: directory in which the model lives
        :type model_dir: str
        :param processor_suite: processing suite which processes every input
                for training and prediction
        :type processor_suite: neuroracer_ai.suites.AbstractProcessorSuite
        """

        ERROR_TXT = "{} parameter can't be None! If you called this " \
                    "constructor manually use the load or create method " \
                    "instead!"

        if model is None:
            raise ValueError(ERROR_TXT.format("model"))
        if name is None:
            raise ValueError(ERROR_TXT.format("name"))
        if model_dir is None:
            raise ValueError(ERROR_TXT.format("model_dir"))
        if processor_suite is None:
            raise ValueError(ERROR_TXT.format("processor_suite"))

        self._model = model  # type: models.AbstractModel
        self._name = name  # type: str
        self._model_dir = model_dir  # type: str
        self._processor_suite = processor_suite  # type: suites.AbstractProcessorSuite

    @staticmethod
    def load(name, model_dir, trainable=True):
        """
        Loads the given model and returns a working instance of the ai class.
        Setting trainable to false decreases the memory usage significantly!
        But should only be used if you only want to predict with an already
        trained nn.
        If you try to train an untrainable nn a Error will be thrown!

        :param: name: name of the ai/ model
        :type name: str
        :param model_dir: directory in which the model lives
        :type model_dir: str
        :param trainable: loads the model in a way which allows it to be trained.
        :type trainable: bool
        :return: constructed AI instance
        :rtype: neuroracer_ai.AI
        """

        ai_parameters = fh.load_ai_parameters(fh.create_config_path(model_dir, name))

        return AI(
            name=name,
            model_dir=model_dir,
            model=KerasModel.load(
                model_path=fh.create_model_path(model_dir, name),
                trainable=trainable),
            **ai_parameters)

    @staticmethod
    def create(name, model_dir, architecture, architecture_params,
               processor_suite=EmptyProcessorSuite()):
        """
        Creates a new model and based on it a new working instance of the ai
        class.

        :param name: name of which the ai/ model uses to save
        :type name: str
        :param model_dir: directory in which the model lives
        :type model_dir: str
        :param architecture: architecture which is used by the model
        :type architecture: Architecture
        :param architecture_params: parameters which are passed to the
                architecture constructor
        :type architecture_params: Dict[str, Any]
        :param processor_suite: processing suite which processes every input
                for training and prediction. If you need something fancier than
                the predefined ones you can use the
                neuroracer_ai.suites.ManualMappingProcessorSuite. It allows you
                to completely customize the routing of the data to the
                specified processors. You should never implement your own
                ProcessorSuite class! This will cause a failure while loading
                the model next time.
        :type processor_suite: neuroracer_ai.suites.AbstractProcessorSuite
        :return: constructed AI instance
        :rtype: neuroracer_ai.AI
        """

        return AI(
            model=KerasModel.create(
                architecture_func=architecture,
                architecture_func_params=architecture_params),
            processor_suite=processor_suite,
            name=name,
            model_dir=model_dir)

    def predict(self, input_data, verbose=0):
        """
        Predicts the values for drive and steering based on the given data.

        :param input_data: a numpy array which contains the input data. The structure of the array depends on the nn.
        :type input_data: numpy.ndarray
        :param verbose: sets the verbosity level of the model 0 = silent,
                1 = progress bar, 2 line per epoch
        :type verbose: int
        :return: a float tuple with the predicted values for drive and steering
        :rtype: Tuple[float, float]
        """

        return self._model.predict(
            s=self._processor_suite.process(input_data),
            verbose=verbose)

    def train(self, train_parameters, verbose=0):
        """
        Trains the model with the given data and returns the history of it.

        :param train_parameters: parameters which should be used for the
                training
        :type train_parameters: ai.TrainParameters
        :param verbose: sets the verbosity level of the model 0 = silent,
                1 = progress bar, 2 line per epoch
        :type verbose: int
        :return: History object of the training
        :rtype: Dict
        """

        train_parameters.train_data_generator = ((self._processor_suite.process(X,True), y)
                                for X, y in train_parameters.train_data_generator)
        train_parameters.validation_data_generator = ((self._processor_suite.process(X,True), y)
                                for X, y in train_parameters.validation_data_generator)

        if train_parameters.use_checkpoints:
            self._write_config(directory=self._model_dir, overwrite=True)

        return self._model.train(
            params=train_parameters,
            checkpoint_path=fh.create_model_path(self._model_dir, self._name),
            verbose=verbose)

    def save(self, directory, overwrite=True):
        """
        Saves the model and the ai config int the given dir.

        :param directory: directory to save
        :type directory: str
        :param overwrite: manages if model files should be overwritten
        :type overwrite: bool
        """

        self._write_config(directory=directory, overwrite=overwrite)
        self._model.save(
            path=fh.create_model_path(directory, self._name),
            overwrite=overwrite)

    def visualize_activation(self, layer_name=None, layer_idx=None, filter_indices=None, seed_input=None):
        """
        Visualizes the activation of the nn and returns it as a numpy array.

        :param layer_name: name of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_name: str
        :param layer_idx: index of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_idx: int
        :param filter_indices: list of indices from filters which get
                visualized. if none all are visualized
        :type filter_indices: List[int]
        :param seed_input: list of images which help the visualization to
                converge
        :type seed_input: np.ndarray
        :return: visualization
        :rtype: np.ndarray
        :raises TypeError: if the backend doesn't support visualization
        """

        if not isinstance(self._model, Debuggable):
            raise TypeError("The chosen backend doesn't support visualization!")

        if seed_input is not None:
            seed_input = self._processor_suite.process(
                data=seed_input,
                use_multithreading=False)

        return self._model.visualize_activation(layer_name=layer_name, layer_idx=layer_idx,
                                                filter_indices=filter_indices, seed_input=seed_input)

    def visualize_saliency(self, layer_name=None, layer_idx=None, filter_indices=None, seed_input=None):
        """
        Visualizes the saliency of the nn and returns it as a numpy array.

        :param layer_name: name of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_name: str
        :param layer_idx: index of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_idx: int
        :param filter_indices: list of indices from filters which get
                visualized. if none all are visualized
        :type filter_indices: List[int]
        :param seed_input: list of images which help the visualization to
                converge
        :type seed_input: np.ndarray
        :return: visualization
        :rtype: np.ndarray
        :raises TypeError: if the backend doesn't support visualization
        """

        if not isinstance(self._model, Debuggable):
            raise TypeError("The chosen backend doesn't support visualization!")

        if seed_input is not None:
            seed_input = self._processor_suite.process(
                data=seed_input,
                use_multithreading=False)

        return self._model.visualize_saliency(layer_name=layer_name, layer_idx=layer_idx,
                                              filter_indices=filter_indices, seed_input=seed_input)

    def get_weights(self, layer_name=None, layer_idx=None):
        """
        Retrieves the weights from a certain layer.

        :param layer_name: name of the layer. if both layer_idx and layer_name
                are given layer_name has priority.
        :param layer_idx: index of the layer
        :return: weights of the layer
        :rtype: np.ndarray
        """

        if not isinstance(self._model, Debuggable):
            raise TypeError("The chosen backend doesn't support visualization!")

        return self._model.get_weights(layer_name=layer_name, layer_idx=layer_idx)

    def get_summary(self):
        """
        Constructs the summary of the structure and returns it as a string.

        :return: summary string of the model
        :rtype: str
        """

        if not isinstance(self._model, Debuggable):
            raise TypeError("The chosen backend doesn't support visualization!")

        return self._model.get_summary()

    def _write_config(self, directory, overwrite):
        """
        Helper method to write the internal state into a config file.
        It prepares the data for the file_handler.create_config_path method

        :param directory:
        :param overwrite:
        """

        # todo: maybe we should make this use some list which contains the field that should get serialized?
        # todo: should we also save the last train parameters?

        data = {
            "ai_parameters": {
                "processor_suite": self._processor_suite
            }
        }

        fh.write_ai_config(
            config_path=fh.create_config_path(directory, self._name),
            overwrite=overwrite,
            data=data)
