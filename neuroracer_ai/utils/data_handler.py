from StringIO import StringIO
import functools
import multiprocessing
from cv2 import imwrite
from os import makedirs, path
import zipfile

from ruamel.yaml import YAML
from tqdm import tqdm

from neuroracer_ai.utils import extracted_data_reader, bag_reader
from neuroracer_ai.utils.config_handler import NB_CPU


def get_data_name(container_name, timestamp, occurrence_counter):
    """
    Creates a unique name for this data.

    :param container_name: The name of the container containing this data
    :type container_name: str
    :param timestamp: The timestamp of the data
    :type timestamp: int
    :param occurrence_counter: a dictionary containing information about
                               the number of occurrences of the data_name
    :type occurrence_counter: dict
    :returns: A new data_name which is unique
    :rtype: str
    """

    data_name = "{container_name}_{timestamp}".format(
        container_name=container_name,
        timestamp=str(timestamp))

    if data_name in occurrence_counter:
        occurrence_counter[data_name] += 1
    else:
        occurrence_counter[data_name] = 0

    counter = occurrence_counter[data_name]

    return "{data_name}_{counter}".format(data_name=data_name, counter=counter)


def write_images(sub_destination, container_name, snapshot_list, verbose=1):
    """
    Writes images into the destination directory.

    :param sub_destination: The directory in which the pictures and the yaml file should be created.
    :type sub_destination: str
    :param container_name: The container_name of the extracted data
    :type container_name: str
    :param snapshot_list: The snapshot_list with the images to write.
    :type snapshot_list: list[neuroracer_ai.utils.ImageSnapshot]
    :param verbose: TODO
    :type verbose: int
    """

    occurence_counter = {}

    if verbose > 0:
        progress_func = functools.partial(tqdm,
                                          desc="writing images into '{}'.".format(sub_destination))
    else:
        def progress_func(x):
            return x

    for snapshot in progress_func(snapshot_list):
        data_name = get_data_name(container_name, snapshot.get_timestamp(), occurence_counter)
        filename = path.join(sub_destination, "{}.jpg".format(data_name))
        imwrite(filename, snapshot.get_uncompressed_image())


def write_images_archive(archive, container_name, snapshot_list, verbose=1):
    """
    Writes images into the destination directory.

    :param archive: archive in which to write the images
    :type archive: zipfile.ZipFile
    :param container_name: The container_name of the extracted data
    :type container_name: str
    :param snapshot_list: The snapshot_list with the images to write.
    :type snapshot_list: list[neuroracer_ai.utils.ImageSnapshot]
    :param verbose: TODO
    :type verbose: int
    """

    occurrence_counter = {}

    if verbose > 0:
        progress_func = functools.partial(tqdm,
                                          desc="writing images into '{}'.".format(archive.filename))
    else:
        def progress_func(x):
            return x

    for snapshot in progress_func(snapshot_list):
        data_name = get_data_name(container_name, snapshot.get_timestamp(), occurrence_counter)
        filename = "{}.jpg".format(data_name)
        archive.writestr(filename, snapshot.get_image().tobytes())


def write_movement_information(sub_destination, container_name, snapshot_list, verbose=1):
    """
    Writes a yaml file with movement information into the destination directory.

    :param sub_destination: The directory in which the pictures and the yaml file should be created.
    :type sub_destination: str
    :param container_name: The container_name of the extracted data
    :type container_name: str
    :param snapshot_list: The snapshot list with the movement information to write.
    :type snapshot_list: list[neuroracer_ai.utils.ImageSnapshot]
    :param verbose: verbosity of this call. 0: no prints, 1: prints
    :type verbose: int
    """

    occurrence_counter = {}
    data = []

    filename = path.join(sub_destination, container_name + ".yaml")

    if verbose > 0:
        print("writing movement information into '{}'.".format(filename))

    for snapshot in snapshot_list:
        data_name = get_data_name(container_name, snapshot.get_timestamp(), occurrence_counter)

        d = {"actions": snapshot.get_actions(),
             "timestamp": str(snapshot.get_timestamp()),
             "image": "{}.jpg".format(data_name)}

        data.append(d)

    yaml = YAML()
    with open(filename, 'w') as outfile:
        # for further information see:
        # https://stackoverflow.com/questions/12470665/how-can-i-write-data-in-yaml-format-in-a-file
        yaml.dump(data, outfile)  # todo: handle flow style


def write_movement_information_archive(archive, container_name, snapshot_list, verbose=1):
    """
    Writes a yaml file with movement information into the destination directory.

    :param archive: archive in which to write the images
    :type archive: zipfile.ZipFile
    :param container_name: The container_name of the extracted data
    :type container_name: str
    :param snapshot_list: The snapshot list with the movement information to write.
    :type snapshot_list: list[neuroracer_ai.utils.ImageSnapshot]
    :param verbose: verbosity of this call. 0: no prints, 1: prints
    :type verbose: int
    """

    occurrence_counter = {}
    data = []

    if verbose > 0:
        print("writing movement information into '{}'.".format(archive.filename))

    for snapshot in snapshot_list:
        data_name = get_data_name(container_name, snapshot.get_timestamp(), occurrence_counter)

        d = {"actions": snapshot.get_actions(),
             "timestamp": str(snapshot.get_timestamp()),
             "image": "{}.jpg".format(data_name)}

        data.append(d)

    filename = "{}.yaml".format(container_name)
    yaml = YAML()
    yaml_buffer = StringIO()
    yaml.dump(data, yaml_buffer)
    archive.writestr(filename, yaml_buffer.getvalue())


def write_snapshot_iterable(destination, container_name, snapshot_list, verbose=1, as_archive=False):
    """
    Writes an snapshot list into a directory specified by <destination>.
    After this function call the directory should contain the pictures
    in the snapshot list as jpg files and a yaml file.

    This function works even if there are snapshots with the same timestamp.
    The snapshots with same timestamp are numbered in the same order as they appear in the snapshot_list.

    :param destination: The directory in which the pictures and the yaml file should be created.
    :param container_name: The prefix of the jpg files
    :param snapshot_list: The snapshot list with the data to write.
    :param verbose: TODO
    :param as_archive: if true snapshot_item will be written into a zip file
    :type as_archive: bool
    """

    if not path.isdir(destination):
        makedirs(destination)

    if not as_archive:
        sub_destination = path.join(destination, container_name)
        if not path.isdir(sub_destination):
            makedirs(sub_destination)

        write_movement_information(sub_destination, container_name, snapshot_list, verbose=verbose)
        write_images(sub_destination, container_name, snapshot_list, verbose=verbose)
    else:
        archive_path = path.join(destination, "{}.zip".format(container_name))
        with zipfile.ZipFile(archive_path, "w", zipfile.ZIP_STORED) as archive:
            write_movement_information_archive(
                archive=archive,
                container_name=container_name,
                snapshot_list=snapshot_list,
                verbose=verbose)
            write_images_archive(
                archive=archive,
                container_name=container_name,
                snapshot_list=snapshot_list,
                verbose=verbose)


def write_snapshot_dict(snapshot_dict, destination, verbose, as_archive=False):
    """
    Writes the given snapshot_dict into the destination directory

    :param snapshot_dict: A dictionary with the container names as keys and the snapshot list as values.
    :param destination: A path to a directory where the extracted data is written
    :param verbose: 0: do not print status information; 1: print status information
    :param as_archive: if true the content of each snapshot_dict is written into zip files
    :type as_archive: bool
    """

    if verbose > 0:
        progress_func = functools.partial(tqdm,
                                          total=len(snapshot_dict),
                                          desc="writing extracted data")
    else:
        def progress_func(x):
            return x

    pool = multiprocessing.Pool(processes=NB_CPU)

    write_snapshot_item_with_arguments = functools.partial(write_snapshot_item,
                                                           destination=destination,
                                                           as_archive=as_archive)

    iterator = pool.imap_unordered(write_snapshot_item_with_arguments, snapshot_dict.items())

    for _ in progress_func(iterator):
        pass

    pool.close()
    pool.join()


def write_snapshot_item(snapshot_item, destination, as_archive):
    """
    Writes the given items into the destination directory

    :param snapshot_item: A tuple (container_name, snapshot_list)
    :param destination: A path to a directory where the extracted data is written
    :param as_archive: if true snapshot_item will be written into a zip file
    :type as_archive: bool
    """

    write_snapshot_iterable(destination=destination,
                            snapshot_list=snapshot_item[1],
                            container_name=snapshot_item[0],
                            verbose=0,
                            as_archive=as_archive)


def snapshot_generator_from_sources(bag_sources, extracted_sources, camera_topic, action_topic,
                                    batch_size=5, frame_skip_bags=0, frame_skip=0,
                                    return_dict=False):
    """
    Constructs an snapshot list from the specified sources.
    If both sources are None the snapshot list is empty.

    :param bag_sources: A list of paths to bag-files or directories which contain bag-files.
                        No bag-files are loaded, if bag_sources is None.
    :type bag_sources: List[str]
    :param extracted_sources: A path to a directory.
                              This directory should contain sub-directories.
                              Every sub-directory should contain a yaml file,
                              that lists all images in the sub-directory
                              and the corresponding actions.
    :type extracted_sources: str
    :param camera_topic: The camera_topic to read from the rosbags.
    :type camera_topic: str
    :param action_topic: The action_topic to read from the rosbags.
    :type action_topic: str
    :param frame_skip_bags: Number of frames to skip after reading a frame from a bag.
                            frame_skip_bags=0: take all frames
                            frame_skip_bags=1: take every second frame
    :param frame_skip_extracted: Number of frames to skip after reading a frame from extracted data.

    :returns: A list of snapshots
    :rtype: List[neuroracer_ai.utils.snapshot.ImageSnapshot]
    :param verbose: TODO
    """

    snapshot_generator = []

    if bag_sources:
        snapshot_generator = bag_reader.read_rosbags_as_generator(bag_sources,
                                                                  batch_size=batch_size,
                                                                  camera_topic=camera_topic,
                                                                  action_topic=action_topic,
                                                                  nb_cpu=NB_CPU,
                                                                  frame_skip=frame_skip_bags,
                                                                  return_dict=return_dict)

    if extracted_sources:
        snapshot_generator = extracted_data_reader.read_data_as_generator(extracted_sources,
                                                                          batch_size=batch_size,
                                                                          frame_skip=frame_skip,
                                                                          nb_cpu=NB_CPU,
                                                                          return_dict=return_dict)

    return snapshot_generator
