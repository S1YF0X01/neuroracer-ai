"""
This module defines functions to augment images and actions
"""

import cv2
import math
import random
import numpy as np


def square(x):
    return x*x


# ------------------ Rotation ------------------

def _calculate_scaling(angle_deg, image_height, image_width):
    """
    Calculates the scaling for an image with shape [image_height, image_width, [depth]]
    so that the rotated image has no borders.

    :param angle_deg: The angle of the image to rotate in degrees
    :param image_height: The height of the original image
    :param image_width: The width of the original
    :return: The float value to use for scaling the image after rotating,
             so that no borders are visible in the rotated image
    :rtype: float
    """
    angle_rad = math.radians(abs(angle_deg))

    half_height = image_height / 2.0
    half_width = image_width / 2.0

    beta = math.atan(half_width / half_height)

    gamma = (beta - angle_rad)

    l = half_height / math.cos(gamma)

    def sqr(x):
        return x*x

    l_l = math.sqrt(sqr(half_height) + sqr(half_width))

    scaling = l_l / l

    return scaling


def augment_rotation(image, actions, intensity):
    """
    Rotates the input image between -intensity and intensity degrees.
    The image size of the output image is equal to the size of the input image.

    Source:
    https://stackoverflow.com/questions/9041681/opencv-python-rotate-image-by-x-degrees-around-specific-point

    :param image: The snapshot to rotate
    :type image: image (uncompressed)
    :param actions: The actions to augment
    :type actions: dict
    :param intensity: Maximal rotation angle in degrees
    :type intensity: float

    :return: A tuple of (image, actions). The image is a rotated image with the same size as the input image.
             The actions are the input actions unchanged.
    :rtype: tuple[image, dict]
    """

    # create a angle between -intensity and intensity
    angle = (random.random() - 0.5) * 2.0 * intensity

    scaling = _calculate_scaling(angle, image.shape[0], image.shape[1])

    image_center = tuple(np.array(image.shape[1::-1]) / 2.0)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, scaling)
    result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)

    return result, actions


# ------------------ Rotation ------------------

def augment_lines(image, actions, number):
    """
    Draws random lines into the image.

    :param image: The image to draw lines in
    :type image: image
    :param number: The number of lines to draw
    :type number: int
    :param actions: The actions of this snapshot (not changed)
    :type actions: dict

    :return: The new image augmented with lines
    :rtype: image
    """

    for i in range(number):
        p1 = (random.randrange(image.shape[1]+100)-50, random.randrange(image.shape[0]+100)-50)
        p2 = (random.randrange(image.shape[1]+100)-50, random.randrange(image.shape[0]+100)-50)

        g = random.randrange(30)
        b = random.randrange(30)
        r = random.randrange(30)

        thickness = random.randrange(1, 3)

        cv2.line(image, p1, p2, (g, b, r), thickness=thickness)
    return image, actions


# ------------------ Brightness ------------------

def augment_brightness(image, actions, intensity):
    """
    Changes the snapshot.image brightness by a random number between -intensity and intensity.

    Source:
    https://stackoverflow.com/questions/32609098/how-to-fast-change-image-brightness-with-python-opencv

    :param image: The image to augment
    :type image: image (uncompressed)
    :param actions: The actions to augment
    :type actions: dict
    :param intensity: Defines how much the image should be
    :type intensity: int

    :return: A tuple of (image, actions). The image is darker or brighter.
             The actions are the input actions unchanged.
    :rtype: tuple[image, dict]
    """

    # create a value from -intensity to intensity
    value = int(random.randrange(intensity * 2 + 1) - intensity)

    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    if value > 0:
        max_lim = 255 - value
        v[v > max_lim] = 255
        v[v <= max_lim] += value
    elif value < 0:
        min_lim = -value
        v[v < min_lim] = 0
        v[v >= min_lim] -= abs(value)

    final_hsv = cv2.merge((h, s, v))
    result = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)

    return result, actions


# ------------------ Flip ------------------

def augment_flip(image, actions):
    """
    Flips the image of the snapshot and inverts the steering value of the snapshot.

    :param image: The image to augment
    :type image: image (uncompressed)
    :param actions: The actions to augment
    :type actions: dict

    :return: A tuple of (image, actions). The image is flipped vertically.
             The actions are the input actions with inverted steer value.
    :rtype: tuple[image, dict]
    """

    result = cv2.flip(image, 1)

    if "steer" not in actions:
        raise KeyError("Key 'steer' not found in actions")
    actions["steer"] = -actions["steer"]
    return result, actions


# ------------------ Blotches ------------------

def augment_blotches(image, actions, nb_blotches, blotch_width, blotch_height):
    """
    Overwrites parts of the image with black blotches.

    :param image: The image to augment
    :type image: image (uncompressed)
    :param actions: The actions to augment
    :type actions: dict
    :param nb_blotches: Number of blotches to add
    :type nb_blotches: int
    :param blotch_width: A tuple with shape (min_blotch_width, max_blotch_width).
                         This will create blotches with min_blotch_width <= blotch_width <= max_blotch_width
    :type blotch_width: tuple[int, int]
    :param blotch_height: A tuple with shape (min_blotch_height, max_blotch_height).
                         This will create blotches with min_blotch_height <= blotch_height <= max_blotch_height
    :type blotch_height: tuple[int, int]

    :return: A tuple of (image, actions). The image is augmented with black blotches.
             The actions are the input actions unchanged.
    :rtype: tuple[image, dict]
    """

    image_height = image.shape[0]
    image_width = image.shape[1]

    for i in range(nb_blotches):
        x = random.randrange(image_width)
        y = random.randrange(image_height)

        w = random.randrange(*blotch_width)
        h = random.randrange(*blotch_height)

        blotch_func = random.choice([blotch_func1, blotch_func2, blotch_func3, blotch_func4])

        add_blotch(image, x, y, w, h, blotch_func)

    return image, actions

# value func should be a function with f(0, 0) == 0, f(1) == 0, f(x) >= 0, f(x) <= 1 for all x with 0 <= x <= 1


def blotch_func1(x):
    return square(1 - abs(2*x - 1))


def blotch_func2(x):
    return (-square(x)+x)*4


def blotch_func3(x):
    return 1 - abs(2*x - 1)


def blotch_func4(x):
    return math.sin(math.pi*x)


# Storage for created blotches to reuse them instead of creating a new one.
blotch_storage = {}
BLOTCH_STORAGE_DIMENSION = 100


def create_blotch(w, h, blotch_func):
    """
    Creates a blotch with the given blotch_function

    :param w: The width of the blotch in pixels.
    :type w: int
    :param h: The height of the blotch in pixels.
    :type h: int
    :param blotch_func: The blotch function (f(float) -> float) to use.
                        This function should have several properties:
                        0 == blotch_func(0) == blotch_func(1)
                        0 <= blotch_func(x) <= 1 for all 0 <= x <= 1
    :type blotch_func: Callable[[float], float]

    :return: A image with a withe blotch.
    :rtype: image
    """

    if blotch_func in blotch_storage:
        blotch = blotch_storage[blotch_func]
    else:
        blotch = np.zeros((BLOTCH_STORAGE_DIMENSION, BLOTCH_STORAGE_DIMENSION, 3), dtype=np.uint8)

        for y in range(BLOTCH_STORAGE_DIMENSION):
            y_norm = y/float(BLOTCH_STORAGE_DIMENSION-1)
            for x in range(BLOTCH_STORAGE_DIMENSION):
                x_norm = x/float(BLOTCH_STORAGE_DIMENSION-1)

                value = blotch_func(x_norm) * blotch_func(y_norm) * 256

                if value < 0:
                    value = 0
                elif value > 255:
                    value = 255

                blotch[y, x] = int(value)
        blotch_storage[blotch_func] = blotch

    rescaled_blotch = cv2.resize(blotch, (w, h))

    return rescaled_blotch


def add_blotch(image, x, y, w, h, value_func):
    """
    Adds a blotch to the image.

    :param image: The image in which to add the blotch
    :type image: image
    :param x: The x position where the blotch should start
    :type x: int
    :param y: The y position where the blotch should start
    :type y: int
    :param w: The width of the blotch
    :type w: int
    :param h: The height of the blotch
    :type h: int
    :param value_func: The value_func of the blotch.
                        See the documentation of create_blotch for more information.
    :type value_func: Callable[[float], float]
    """
    image_height = image.shape[0]
    image_width = image.shape[1]

    blotch = create_blotch(w, h, blotch_func=value_func)

    y_end = y+h
    if y_end >= image_height:
        y_end = image_height-1
        blotch = blotch[:y_end-y, :]

    x_end = x+w
    if x_end >= image_width:
        x_end = image_width-1
        blotch = blotch[:, :x_end-x, :]

    image_area = image[y:y_end,x:x_end,:]
    blotch = np.fmin(blotch, image_area)
    image_area[:] -= blotch
