import os
import time

import inspect


def log_history(history, architecture_func, model_name, destination):
    """
    Dumps a history object to the given destination directory

    :param history: The history object to dump
    :param architecture_func: The architecture function used by the model
    :param destination: The name of the log file, where to write the information
    :param model_name: The name of the model

    TODO: Write more information about history. Maybe dump the history.history dict.
    """

    if destination is "" or destination is None:
        return

    # get source code of the architecture
    if architecture_func is not None:
        architecture_code = inspect.getsource(architecture_func)

    stats = table_from_dict(history.history)

    data = []

    data.append("model_name:")
    data.append(model_name)
    data.append("")
    data.append("timestamp:")
    data.append(time.strftime("%d-%m-%Y %H:%M:%S"))
    data.append("")
    data.append("metrics:")
    data.extend(stats)
    data.append("")
    if architecture_func is not None:
        data.append("architecture:")
        data.append(architecture_code)

    if os.path.isfile(destination):
        with open(destination, "a") as f:
            f.write("\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\n")
            for line in data:
                f.write(line)
                f.write("\n")
    else:
        with open(destination, "w") as f:
            for line in data:
                f.write(line)
                f.write("\n")


def table_from_dict(d):
    """
    Creates a list of strings of a table with one column for each key.

    :param d: A dictionary. d.values() has to contain iterables for the values in the table.
    :type d: dict[iterable]
    :return: List of strings of a table.
    :rtype: list[str]
    """

    clean_dict = {}

    longest_values = []

    number_rows = 0

    for key, value in d.items():
        # key type check
        if type(key) is not str:
            continue

        # iterable check
        try:
            iterator = iter(value)
        except TypeError:
            continue

        i_len = len(value)
        if i_len > number_rows:
            number_rows = i_len

        longest_value = len(key)
        for i in iterator:
            s_len = len(str(i))
            if s_len > longest_value:
                longest_value = s_len
        longest_values.append(longest_value)
        clean_dict[key] = value

    table_width = 0
    for i in longest_values:
        table_width += (i + 3)

    line = ("{0:-^" + str(table_width) + "}").format("")

    table = []

    row = ""

    for key, longest_value in zip(clean_dict.keys(), longest_values):
        row = row + ("{:^" + str(longest_value + 2) + "}|").format(key)

    table.append(row[:-1])
    table.append(line)

    for i in range(number_rows):
        row = ""
        for v, longest_value in zip(clean_dict.values(), longest_values):
            row = row + ("{:^" + str(longest_value + 2) + "}|").format(v[i])
        table.append(row[:-1])

    return table
