import sys


class SharedValueDict:
    """
    A dictionary wrapper that holds every key reference only once.
    Even when an instance of this class is dumped and reloaded, it only contains one reference to each value.
    """

    def __init__(self, original=None):
        """
        Initiates a SharedValueDict.

        :param original: A dictionary to copy
        :type original: dict
        """
        self.key_to_index = {}
        self.index_to_value = []

        if original:
            self.update(original)

    def __len__(self):
        """
        Returns the number of keys in this dictionary.

        :returns: The number of keys in this dictionary
        :rtype: int
        """
        return len(self.key_to_index)

    def __getitem__(self, key):
        """
        Gets an item specified by the given key.

        :param key: The key
        :type key: Any
        :returns: The value specified by the key.
        :rtype: Any
        """

        return self.index_to_value[self.key_to_index[key]]

    def __setitem__(self, key, value):
        """
        Sets the item specified by the given key.
        Every value is saved only once in this dictionary.

        :param key: The key. Must be immutable
        :type key: Any
        :param value: A value
        :type value: Any
        """

        new_index = len(self.index_to_value)
        if value in self.index_to_value:
            new_index = self.index_to_value.index(value)
        else:
            self.index_to_value.append(value)

        self.key_to_index[key] = new_index

    def __iter__(self):
        """
        Returns an iterator over the keys of this dictionary.

        :returns: An iterator over the keys of this dictionary.
        :rtype: dictionary-keyiterator
        """

        return self.key_to_index.__iter__()

    def __eq__(self, other):
        """
        Returns True if self is equal to other otherwise False

        :param other: The dictionary to check equality with.
        :type other: SharedValueDict
        :returns: True if self is equal to other otherwise False
        :rtype: bool
        """

        if isinstance(other, SharedValueDict):
            return self.key_to_index == other.key_to_index and self.index_to_value == other.index_to_value
        return False

    def __ne__(self, other):
        """
        Returns True if self is unequal to other otherwise False

        :param other: The dictionary to check unequality with.
        :type other: SharedValueDict
        :returns: True if self is unequal to other otherwise False
        :rtype: bool
        """

        return not self.__eq__(other)

    def keys(self):
        """
        Returns a list of all keys of this dictionary.

        :returns: A list of all keys of this dictionary.
        :rtype: List[Any]
        """
        return self.key_to_index.keys()

    def values(self):
        """
        Returns a list of all values of this dictionary.

        :returns: A list of all values of this dictionary.
        :rtype: List[Any]
        """

        return self.index_to_value

    def items(self):
        """
        Returns a list of all items of this dictionary.

        :returns: A list of all items of this dictionary.
        :rtype: List[Tuple[Any, Any]]
        """

        assert sys.version_info < (3,)
        items = []
        for k in self.key_to_index:
            items.append((k, self[k]))
        return items

    def has_key(self, key):
        """
        Returns True if the specified key is in the keys of this dictionary otherwise False.

        :param key: The key to look for.
        :type key: Any
        :returns: True if the specified key is in the keys of this dictionary otherwise False.
        :rtype: bool
        """

        return self.key_to_index.has_key(key)

    def get(self, key):
        """
        Returns the value specified by the given key.

        :param key: The key to look for.
        :type key: Any
        :returns: The value associated with the given key.
        :rtype: Any

        :raises KeyError: If the given key is not this dictionary
        """

        return self.index_to_value[self.key_to_index[key]]

    def clear(self):
        """
        Clears all keys and all values of this dict.
        """

        self.index_to_value = []
        self.key_to_index.clear()

    def iterkeys(self):
        """
        Returns an iterator over the keys of this dictionary.

        :returns: An iterator over the keys of this dictionary.
        :rtype: dictionary-keyiterator
        """

        return self.key_to_index.iterkeys()

    def itervalues(self):
        """
        Returns an iterator over the values of this dictionary.

        :returns: An iterator over the values of this dictionary.
        :rtype: dictionary-valueiterator
        """

        return self.index_to_value.__iter__()

    def iteritems(self):
        """
        Returns an iterator over the items of this dictionary.

        :returns: An iterator over the items of this dictionary.
        :rtype: dictionary-itemiterator
        """

        for i in self.items():
            yield i

    def copy(self):
        """
        Creates a shallow copy of this dictionary.

        :returns: A shallow copy of this dictionary.
        :rtype: SharedValueDict
        """

        svd = SharedValueDict()
        svd.index_to_value = self.index_to_value
        svd.key_to_index = self.key_to_index
        return svd

    def update(self, other):
        """
        Adds the given dictionary to this dictionary.

        :param other: The dictionary to add to this dictionary.
        :type other: dict
        """

        for key, value in other.iteritems():
            self.__setitem__(key, value)
