from abc import ABCMeta, abstractmethod

import cv2

CV_LOAD_IMAGE_AS_IS = -1


class AbstractSnapshot:
    """
    Defines a snapshot representing a state and actions.
    The actions must be added by subclasses.
    """

    __metaclass__ = ABCMeta

    def __init__(self, actions, timestamp):
        """
        Initiates a Snapshot.

        :param actions: A dictionary specifying the action's values.
        :type actions: dict
        :param timestamp: The timestamp when the snapshot was recorded.
        :type timestamp: int
        """

        self._actions = actions
        self._timestamp = timestamp

    def get_actions(self):
        """
        Returns the actions of this snapshot as dictionary.

        :returns: A dictionary of actions
        :rtype: dict[str, Any]
        """

        return self._actions


    # hack to get the system working with only steer as return value
    #def get_actions_as_tupel(self, keys=("steer", "drive")):
    def get_actions_as_tupel(self, keys=("steer", )):
        """
        Returns values as tuple in this event class.

        :param keys: A tuple of keys defining the order of the returned tuple
        :type keys: tuple
        :returns: A tuple of actions in the same order as keys.
        :rtype: tuple
        """

        value_list = []
        for key in keys:
            value_list.append(self._actions[key])

        return tuple(value_list)

    def get_nb_actions(self):
        """
        Returns the number of actions in this event class.

        :returns: The number of actions in this event class.
        :rtype: int
        """

        return len(self._actions)

    def get_timestamp(self):
        """
        Returns the timestamp from the moment the snapshot was recorded.

        :returns: The timestamp from the moment the snapshot was recorded.
        :rtype: int
        """

        return self._timestamp

    @abstractmethod
    def update_snapshot(self, actions, *args):
        """
        TODO

        :param actions: TODO
        :param args: TODO
        :return: TODO
        """

        pass


class ImageSnapshot(AbstractSnapshot):
    """
    Defines a snapshot with one image as state.
    The actions are (steer, drive)
    """

    def __init__(self, actions, timestamp, image=None, uncompressed_image=None):
        """
        Initiates a ImageSnapshot.

        :param actions: The actions for this Snapshot.
        :type actions: dict
        :param timestamp: The timestamp when the snapshot was recorded.
        :type timestamp: int
        :param image: The image for this Snapshot as opencv compressed image.
        :type image: opencv compressed image
        :param uncompressed_image: The image for this Snapshot as opencv uncompressed image.
        :type image: opencv image
        """

        AbstractSnapshot.__init__(self, actions, timestamp)

        if image is not None:
            self._image = image
        elif uncompressed_image is not None:
            self._image = self._compress_image(uncompressed_image)
        else:
            raise ValueError("You have to pass at least image or uncompressed_image")

    @staticmethod
    def copy(other, actions=None, timestamp=None, compressed_image=None, uncompressed_image=None):
        """
        Copies the other ImageSnapshot but overwrites the given values except they are None.

        :param other: The ImageSnapshot to copy.
        :type other: ImageSnapshot
        :param actions: The actions to copy
        :type actions: dict
        :param compressed_image: The image to copy
        :type compressed_image: compressed_image
        :param uncompressed_image: The image to copy
        :type uncompressed_image: image
        :return: A copy of other with the given values.
        :rtype: ImageSnapshot
        """
        _actions = other.get_actions()
        _compressed_image = other.get_image()
        _timestamp = other.get_timestamp()

        if actions is not None:
            _actions = actions
        if timestamp is not None:
            _timestamp = timestamp
        if compressed_image is not None:
            _compressed_image = compressed_image
        elif uncompressed_image is not None:
            _compressed_image = ImageSnapshot._compress_image(uncompressed_image)

        return ImageSnapshot(actions=_actions,
                             timestamp=_timestamp,
                             image=_compressed_image)

    def get_image(self):
        """
        Returns the image of this ImageSnapshot.

        :returns: The image of this ImageSnapshot.
        :rtype: opencv compressed image
        """

        return self._image

    def get_uncompressed_image(self):
        """
        Returns the uncompressed image of this ImageSnapshot.

        :returns: The uncompressed image of this ImageSnapshot.
        :rtype: np.nd.array
        """

        return cv2.imdecode(self._image, flags=CV_LOAD_IMAGE_AS_IS)

    @staticmethod
    def _compress_image(image, ext='.jpg'):
        """
        Compresses an image.

        :returns: The compressed image.
        :rtype: TODO
        """

        return cv2.imencode(ext, image)[1]

    def update_snapshot(self, actions=None, image=None, image_compress=False):
        """
        TODO

        :param actions: TODO
        :param image: TODO
        :param image_compress: TODO
        :return: TODO
        """

        if actions is not None:
            AbstractSnapshot.actions = actions
        if image is not None:
            if image_compress:
                self._image = ImageSnapshot._compress_image(image)
            else:
                self._image = image
