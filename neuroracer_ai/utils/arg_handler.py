import argparse


def get_argparser(description):
    """
    Creates the ArgumentParser and returns common parsed arguments.

    :returns: A dictionary with the parsed command line arguments.
    """

    parser_common = argparse.ArgumentParser(add_help=False, description=description,
                                            formatter_class=argparse.RawTextHelpFormatter)

    # configfile
    config = parser_common.add_argument_group('config')
    config.add_argument("--configfile", type=str, help="A path to a configuration file in yaml syntax")

    # monitoring
    debug = parser_common.add_argument_group('debug')
    debug.add_argument("--verbose", type=int, help="Level of verbosity. 0: no output; 1: output")

    # topics
    topics = parser_common.add_argument_group('topics')
    topics.add_argument("--camera_topic", type=str, help="The camera topic to read from")
    topics.add_argument("--action_topic", type=str, help="The actions topic to read from")

    return parser_common
