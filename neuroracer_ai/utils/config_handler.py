from multiprocessing import cpu_count
from warnings import warn

from ruamel.yaml import Loader, load, parser

MAX_NB_CPU = 8  # TODO make this configurable
NB_CPU = cpu_count() if cpu_count() < MAX_NB_CPU else MAX_NB_CPU


def read_config_file(config_file_path):
    """
    Reads a specified config file in yaml format and returns a dictionary with the configuration.

    :raises IOError: If config file was not available.
    :raises yaml.parser.ParserError: If the config file is not a valid yaml file.
    :returns: a dictionary with the configuration data.
    """

    try:
        with open(config_file_path, 'r') as config_file:
            config = load(config_file, Loader=Loader)
            return config
    except IOError:
        raise IOError("Failed to open '{}'".format(config_file_path))
    except parser.ParserError:
        raise IOError("Failed to parse '{}'. Probably not a valid yaml file.".format(config_file_path))


def check_config(config, and_required_keys=None, or_required_keys=None):
    """
    Checks whether the configuration has all needed fields or not.

    :param config:  A dictionary with configuration data in it.
                    Example keys: "sources", "destination", "camera_topic", "action_topic".
    :param and_required_keys: A dictionary with configuration data in it.
                    Example keys: "sources", "destination", "camera_topic", "action_topic".
    :param or_required_keys: A dictionary with configuration data in it with or-required tuples.
                    Example keys: "sources", "destination", "camera_topic", "action_topic".
    """

    if and_required_keys is None:
        warn("No keys as explicit requirements defined.", UserWarning, stacklevel=2)
    else:
        for key in and_required_keys:
            if key not in config:
                raise KeyError(
                    "Key '{}' not found in configuration. Specify this parameter via command line arguments (--help for help) or define a configfile.".format(
                        key))

    if or_required_keys:
        for tmp_tuple in or_required_keys:
            has_key = False
            for key in tmp_tuple:
                if key in config:
                    has_key = True
                    break

            if not has_key:
                raise KeyError(
                    "None of the following keys '{}' were found in configuration. Specify one of these parameters via command line arguments (--help for help) or define a configfile.".format(
                        tmp_tuple))
