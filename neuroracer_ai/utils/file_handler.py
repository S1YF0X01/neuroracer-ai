import os
from cv2 import imwrite
from os import path

from ruamel.yaml import YAML
from ruamel.yaml.scanner import ScannerError
from tqdm import tqdm


def create_model_path(directory, model_name):
    """
    Combines the given directory, model_name and the from the function defined
    extension into a full path.

    :param directory: first part of the path
    :type directory: str
    :param model_name: second part of the path
    :type model_name: str
    :return: combination of the directory and model_name
    :rtype: str
    """

    return os.path.join(directory, "{}.hdf5".format(model_name))


def create_config_path(directory, model_name):
    """
    Combines the given directory, model_name and the from the function defined
    extension into a full path.

    :param directory: first part of the path
    :type directory: str
    :param model_name: second part of the path
    :type model_name: str
    :return: combination of the directory and model_name
    :rtype: str
    """

    return os.path.join(directory, "{}.nrai".format(model_name))


def write_ai_config(config_path, overwrite, data):
    """
    Dumps the data in form of a YAML file at the config_path location.

    :param config_path: path at which the files get written
    :type config_path: str
    :param overwrite: enables overwriting of the file
    :type overwrite: bool
    :param data: data to write
    :type data: Dict[str, Any]
    """

    directory, _ = os.path.split(config_path)
    if directory and not os.path.exists(directory):
        os.makedirs(directory)

    if not overwrite and os.path.exists(config_path):
        raise IOError("AI config file (\"{}\") already exists and overwriting it is forbidden!".format(config_path))

    yaml = YAML(typ="unsafe")
    try:
        with open(os.path.expanduser(config_path), "w") as output_file:
            yaml.dump(data, stream=output_file)  # todo: handle flow style
    except IOError as e:
        raise IOError("AI config file (\"{}\") couldn't be written!\nERROR: {}".format(config_path, str(e)))
    except ScannerError as e:
        raise ValueError("AI config file (\"{}\") couldn't be dumped to YAML!\nERROR: {}".format(config_path, str(e)))


def load_ai_config(config_path):
    """
    Loads and returns the the configuration as dictionary.

    :param config_path: The path to the configuration file.
    :type config_path: str
    :returns: The configuration as dictionary
    :rtype: Dict[str, Any]

    :raises IOError: If the config file could not be opened.
    :raises ValueError: If the config file could not be parsed.
    """

    if not config_path:
        raise ValueError("path can't be None or empty!")

    yaml = YAML(typ="unsafe")
    try:
        with open(os.path.expanduser(config_path), "r") as input_file:
            config = yaml.load(input_file)
    except IOError as e:
        raise IOError("AI config file (\"{}\") couldn't be loaded!\nERROR: {}".format(config_path, str(e)))
    except ScannerError as e:
        raise ValueError("AI config file (\"{}\") couldn't be parsed!\nERROR: {}".format(config_path, str(e)))

    return config


def load_ai_parameters(config_path):
    """
    Loads the ai parameters from the config file.
    This config file has to contain the key "ai_parameters".

    :param config_path: Path to the configfile
    :type config_path: str
    :returns: A dictionary with the ai_parameters
    :rtype: dict

    :raises IOError: If the config file could not be opened.
    :raises ValueError: If the config file could not be parsed.
    """

    config = load_ai_config(config_path)

    if "ai_parameters" not in config:
        return {}

    return config["ai_parameters"]


def _progress_func(x):
    """
    Wrapper. Required by PEP8

    :param x: TODO
    :return: TODO
    """

    return x


def write_images(sub_destination, container_name, event_list, verbose=1):
    """
    Writes images into the destination directory.

    :param sub_destination: The directory in which the pictures and the yaml file should be created.
    :type sub_destination: str
    :param container_name: The container_name of the extracted data
    :type container_name: str
    :param event_list: The event_list with the images to write.
    :type event_list: list[neuroracer_ai.utils.Event]
    :param verbose: TODO
    :type verbose: int
    """

    if verbose > 0:
        print("writing images into '{}'.".format(sub_destination))
        progress_func = tqdm
    else:
        progress_func = _progress_func
    for event in progress_func(event_list):
        filename = path.join(sub_destination, "{}_{}.jpg".format(container_name, str(event.timestamp)))
        imwrite(filename, event.state)
