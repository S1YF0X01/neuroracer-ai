#!/usr/bin/env python
import multiprocessing
import os
import sys
import logging
import functools
from tqdm import tqdm

import cv2
import rosbag
import numpy as np
from cv_bridge import CvBridge
from rospy.rostime import Time

from neuroracer_ai.utils.snapshot import ImageSnapshot


logger = logging.getLogger("pymp")
logger.addHandler(logging.StreamHandler(sys.stdout))

IMAGE_FILE_EXTENSION = ".jpg"


class ActionMessage:
    def __init__(self, actions):
        self.actions = actions


class CameraEvent:
    def __init__(self, timestamp, state):
        self.timestamp = timestamp
        self.state = state


class ActionEvent:
    def __init__(self, timestamp, actions):
        self.timestamp = timestamp
        self.actions = actions


def get_container_name(rosbag_path):
    """
    Transforms paths like 'res/example.bag' into 'example'.

    :param rosbag_path: The path to a rosbag to transform.
    :returns: The container_name of a rosbag path.
    """

    return os.path.splitext(os.path.basename(rosbag_path))[0]


def read_rosbag_list(bag_sources, camera_topic=None, action_topic=None, verbose=1, nb_cpu=1, frame_skip=0):
    """
    Reads all rosbags in bag_sources and puts them as into a list of snapshots.

    :param bag_sources: list of sources from where to read bags
                        (path to bag files or path to directories, which contain bag files)
    :type bag_sources: list[str]
    :param action_topic: The action topic to read from
    :type action_topic: str
    :param camera_topic: Camera topic to extract from the bag.
    :type camera_topic: str
    :param verbose: 0: do not print anything; 1: print status information
    :type verbose: int
    :param frame_skip: Number of frames to skip after reading a frame.
                       frame_skip=0: take all frames
                       frame_skip=1: take every second frame
   :param nb_cpu: The number of cpus to use
   :type nb_cpu: int

    :returns: A list of snapshots.
    """
    snapshot_list = []
    snapshot_dict = read_rosbag_dict(bag_sources=bag_sources,
                                     camera_topic=camera_topic,
                                     action_topic=action_topic,
                                     verbose=verbose,
                                     nb_cpu=nb_cpu,
                                     frame_skip=frame_skip)

    for i in snapshot_dict.values():
        snapshot_list.extend(i)

    return snapshot_list


def read_rosbag_dict(bag_sources, camera_topic=None, action_topic=None, verbose=1, nb_cpu=1, frame_skip=0):
    """
    Reads all rosbags in bag_sources and puts them as snapshot lists into a dict with the filename as key.

    :param bag_sources: list of sources from where to read bags
                        (path to bag files or path to directories, which contain bag files)
    :type bag_sources: list[str]
    :param action_topic: The action topic to read from
    :type action_topic: str
    :param camera_topic: Camera topic to extract from the bag.
    :type camera_topic: str
    :param verbose: 0: do not print anything; 1: print status information
    :type verbose: int
    :param frame_skip: Number of frames to skip after reading a frame.
                       frame_skip=0: take all frames
                       frame_skip=1: take every second frame
   :param nb_cpu: The number of cpus to use
   :type nb_cpu: int

    :returns: A dictionary. The key is a path to a file and the values are lists of
              snapshots loaded from the bag in key
    """

    rosbag_dict = {}

    # list of paths to rosbags in bag_sources
    bag_list = get_rosbag_list(bag_sources)

    if verbose > 0:
        progress_function = functools.partial(tqdm,
                                              total=len(bag_list),
                                              desc="Reading rosbags: ")
    else:
        def progress_function(x):
            return x

    # if multiprocessing is necessary
    if nb_cpu > 1 and len(bag_list) > 1:
        pool = multiprocessing.Pool(processes=nb_cpu)

        read_rosbag_with_arguments = functools.partial(read_rosbag,
                                                       camera_topic=camera_topic,
                                                       action_topic=action_topic,
                                                       verbose=0,  # verbose 0 because multiprocessing
                                                       frame_skip=frame_skip)

        iterator = pool.imap(read_rosbag_with_arguments, bag_list)

        for bag_path, snapshot_list in zip(progress_function(bag_list), iterator):
            rosbag_dict[bag_path] = snapshot_list

        pool.close()
        pool.join()

    else:
        for i in bag_list:
            rosbag_dict[i] = read_rosbag(i,
                                         camera_topic=camera_topic,
                                         action_topic=action_topic,
                                         verbose=verbose,
                                         frame_skip=frame_skip)

    return rosbag_dict

def read_rosbags_as_generator(bag_sources, batch_size, shuffle=False, camera_topic=None,
                              action_topic=None, nb_cpu=1, frame_skip=0, return_dict=False,
                              loop_indefinitely=True):
    """
    Reads all rosbags in bag_sources and yields them as sourcepath/snapshot-tuple batches as
    a generator.

    :param bag_sources: list of sources from where to read bags
                        (path to bag files or path to directories, which contain bag files)
    :type bag_sources: list[str]
    :param batch_size: size of the batches the generator will yield
    :type
    :param action_topic: The action topic to read from
    :type action_topic: str
    :param camera_topic: Camera topic to extract from the bag.
    :type camera_topic: str
    :param verbose: 0: do not print anything; 1: print status information
    :type verbose: int
    :param frame_skip: Number of frames to skip after reading a frame.
                       frame_skip=0: take all frames
                       frame_skip=1: take every second frame
   :param nb_cpu: The number of cpus to use
   :type nb_cpu: int

    :returns: A list of snapshot objects.
    """

    def get_snapshot_list(rosbag_paths_batch):
        if nb_cpu > 1:
            return get_snapshot_list_multiprocessing(rosbag_paths_batch)
        else:
            return get_snapshot_list_singleprocess(rosbag_paths_batch)

    def get_snapshot_list_singleprocess(rosbag_paths_batch):
        return [read_rosbag(bag,
                            camera_topic=camera_topic,
                            action_topic=action_topic,
                            verbose=0,
                            frame_skip=frame_skip)
               for bag in rosbag_paths_batch]

    def get_snapshot_list_multiprocessing(rosbag_paths_batch):
        pool = multiprocessing.Pool(processes=nb_cpu)
        read_rosbag_multi = functools.partial(read_rosbag,
                                              camera_topic=camera_topic,
                                              action_topic=action_topic,
                                              verbose=0,
                                              frame_skip=frame_skip)
        snapshot_lists_batch = pool.imap(read_rosbag_multi, rosbag_paths_batch)
        pool.close()
        pool.join()
        return snapshot_lists_batch

    def flatten_snapshot_lists(snapshot_batch):
        return [snap for snap_list in snapshot_batch for snap in snap_list]

    def snapshot_lists_to_dict(snapshot_batch):
        return {bag_path: snap_list
                for bag_path, snap_list
                in zip(rosbag_paths_batch, snapshot_batch)}

    def index_needs_reset():
        return loop_indefinitely and idx + batch_size >= len(rosbag_paths)


    if camera_topic is None:
        raise ValueError("camera_topic is None")

    if action_topic is None:
        raise ValueError("action_topic is None")

    rosbag_paths = sorted(get_rosbag_list(bag_sources))
    idx = 0

    while idx < len(rosbag_paths):
        rosbag_paths_batch = rosbag_paths[idx:idx+batch_size]
        idx = 0 if index_needs_reset() else idx+batch_size

        if index_needs_reset() and shuffle:
            np.random.shuffle(rosbag_paths)

        snapshot_batch = get_snapshot_list(rosbag_paths_batch)

        if return_dict:
            yield snapshot_lists_to_dict(snapshot_batch)
        else:
            yield flatten_snapshot_lists(snapshot_batch)


def get_all_files_in(path, filter_function=lambda x: True):
    """
    Walks through a directory and returns a list of all files in this directory.

    :param filter_function: TODO
    :param path: the directory to walk through.
    :returns: A list of all files in this directory.
    """
    files = []
    for (dirpath, dirnames, filenames) in os.walk(path):
        for filename in filenames:
            if filter_function(filename):
                files.append(os.path.join(dirpath, filename))

    return files


def get_rosbag_list(sources):
    """
    Returns a list of all rosbags found in sources.

    :param sources: A list containing paths to rosbags or paths to directories containing rosbags.
    :returns: A list of paths to all .bag files specified by sources or found in a directory in sources.
    """

    source_paths = set()

    for s in sources:
        if os.path.isfile(s):
            source_paths.add(s)
        elif os.path.isdir(s):
            source_paths = source_paths | set(get_all_files_in(s, lambda x: x.endswith(".bag")))
        else:
            raise ValueError("'{}' in sources not found.".format(s))

    return list(source_paths)


def nearer(original, time, reference):
    """
    Detects whether time or reference is nearer to original.

    :param original: The ros-time you want to find the nearest neighbor.
    :param time: If time is nearer to original than reference, the functions returns True.
    :param reference: If time is not nearer to original than reference, the functions returns False.
    :returns: True if <time> is nearer to original than <reference>, otherwise False.
    """

    return abs(original - time) < abs(original - reference)


def handle_action_event(action_msg, time, camera_events, last_action, snapshot_list):
    """
    Adds the action_msg, time and camera event into the snapshot list.

    :param action_msg: The message of the current action event
    :type action_msg: ActionMessage
    :param time: The time of the current action event.
    :param camera_events: The list of all unassigned camera_events with shape (timestamp, camera_msg).
    :param last_action: The last action event before the current action event.
    :param snapshot_list: The list of all snapshots.
    """

    # Loop over all camera events which are not yet in the snapshot_list and older than the current action event
    for camera_event in camera_events:
        # If current action event is closer to the camera event than the last action event
        if last_action is None or nearer(camera_event.timestamp, time, last_action.timestamp):
            snapshot = ImageSnapshot(actions=action_msg.actions,
                                     timestamp=camera_event.timestamp,
                                     image=camera_event.state)
            snapshot_list.append(snapshot)

        # If last action event is closer to the camera event than the current action event
        else:
            snapshot = ImageSnapshot(actions=last_action.actions,
                                     timestamp=camera_event.timestamp,
                                     image=camera_event.state)
            snapshot_list.append(snapshot)

    # clear the camera_events
    del camera_events[:]


# The action type transformer. Used to transform action types into a tuple of shape (steer, drive)
# TODO make generic/configurable
action_type_transformer = {
    "geometry_msgs/Vector3": lambda msg: ActionMessage({'drive': msg.x, 'steer': msg.z}),
    "geometry_msgs/Twist": lambda msg: ActionMessage({'drive': msg.linear.x, 'steer': msg.angular.z})
}


def transform_action_msg(msg, action_type):
    """
    Converts a message into actions.
    If your action type is not supported, add a transformation into the action_type_transformer above.
    """

    if action_type not in action_type_transformer:
        # If you want to support a action type add a converter function into the action_type_transformer above.
        raise KeyError("The action type '{}' is not supported.".format(action_type))
    return action_type_transformer[action_type](msg)


def get_topic_type(bag, topic):
    """
    Gets the type of a topic in a specified bag.

    :param bag: The bag in which the topic should appear.
    :param topic: Specifies the topic whose type is returned.
    :returns: The string representation of the type of the topic.
    """

    return bag.get_type_and_topic_info()[1][topic][0]


cv_bridge = CvBridge()


def _progress_function(x):
    return x


def read_rosbag(path_to_rosbag, camera_topic, action_topic, verbose, frame_skip=0):
    """
    Reads a rosbag with specified camera and action topic and adds these in a snapshot_list list.

    :param path_to_rosbag: The path to the rosbag to read from
    :param camera_topic: The topic on which the camera posted the images
    :param action_topic: The topic on which the controller posted the action data
    :param verbose: 1: print status information to stdout; 0: do not print status information to stdout
    :param frame_skip: Number of frames to skip after reading a frame.
                       frame_skip=0: take all frames
                       frame_skip=1: take every second frame

    :returns: A list of ImageSnapshots. 
    :rtype: List[utils.ImageSnapshot]
    """

    if camera_topic is None:
        raise ValueError("camera_topic is None")

    if action_topic is None:
        raise ValueError("action_topic is None")

    try:
        bag = rosbag.Bag(path_to_rosbag)
    except rosbag.bag.ROSBagUnindexedException:
        raise IOError("Unindexed bag '{}'".format(path_to_rosbag))

    # The message type of the action topic
    action_type = get_topic_type(bag, action_topic)

    # list of all snapshots (The return value of this function)
    snapshot_list = []

    # temporarily list of all images received on the camera_topic
    camera_events = []

    # last action received on the action_topic. Has shape (timestamp, (drive, steer))
    last_action = None

    # The timestamp pointing to the time where the last event occurred
    last_time = Time(0)

    skip_counter = 0

    if verbose > 0:
        number_of_messages = bag.get_message_count(camera_topic) + bag.get_message_count(action_topic)
        progress_function = functools.partial(
            tqdm,
            total=number_of_messages,
            desc="Reading rosbag '{}'.".format(path_to_rosbag))
    else:
        progress_function = _progress_function

    for topic, msg, time in progress_function(bag.read_messages(topics=[camera_topic, action_topic])):
        if topic == camera_topic:
            if skip_counter == 0:
                # transform ros image message to cv2 compressed image
                cv_image = cv_bridge.compressed_imgmsg_to_cv2(msg)
                _, cv_image_compressed = cv2.imencode(IMAGE_FILE_EXTENSION, cv_image)
                event = CameraEvent(time, cv_image_compressed)
                camera_events.append(event)

            # increase skip_counter by one and limit skip_counter to frame_skip
            skip_counter = (skip_counter + 1) % (frame_skip + 1)

        elif topic == action_topic:
            # Transform msg into a tuple of shape (drive, steer)
            msg = transform_action_msg(msg, action_type)

            # add the camera events with the nearest action event into the snapshot_list
            handle_action_event(msg, time, camera_events, last_action, snapshot_list)

            # reset last_action
            last_action = ActionEvent(time, actions=msg.actions)

        # assert that the last event is older then the current event
        if last_time > time:
            raise AssertionError('Events in bag are not in chronological order.')

    if last_action is None:
        # If this error raises you probably have specified a wrong action or camera topic
        raise AssertionError('last_action was never set.')

    # add remaining camera events
    for camera_event in camera_events:
        snapshot = ImageSnapshot(actions=last_action.actions,
                                 timestamp=camera_event.timestamp,
                                 image=camera_event.state)
        snapshot_list.append(snapshot)

    bag.close()

    return snapshot_list
