#!/usr/bin/env python
import os
import cv2
import multiprocessing
import numpy as np

from collections import defaultdict
from ruamel.yaml import YAML

from neuroracer_ai.utils.config_handler import NB_CPU
from neuroracer_ai.utils.snapshot import ImageSnapshot

IMAGE_FILE_EXTENSION = ".jpg"


def read_data_as_generator(main_dir, batch_size, shuffle=False, frame_skip=0, nb_cpu=1,
                           return_dict=False, loop_indefinitely=True):

    def split_snapshot_data_and_subdir_names(snapshot_data):
        snapshot_data_batch, sub_dir_names_batch = [], []

        for sublist in snapshot_data:
            for sub_dir_name, action, timestamp, img_path in sublist:
                snapshot_data_batch.append((action, timestamp, img_path))
                sub_dir_names_batch.append(sub_dir_name)

        return snapshot_data_batch, sub_dir_names_batch

    def make_snapshots_from_snapshot_data(snap_data):
        if nb_cpu > 1:
            pool = multiprocessing.Pool(processes=NB_CPU)
            snapshots = pool.imap(make_snapshot_from_data, snap_data)
            pool.close()
            pool.join()
            return snapshots
        else:
            return [make_snapshot_from_data(snap) for snap in snap_data]

    def group_snapshots_by_dir_name(dirs, snaps):
        snapshots_grouped_by_subdir = defaultdict(list)
        for dir, snap in zip(dirs, snaps):
            snapshots_grouped_by_subdir[dir].append(snap)
        return snapshots_grouped_by_subdir

    def index_needs_reset():
        return loop_indefinitely and idx + batch_size >= len(sub_dirs)

    if not os.path.isdir(main_dir):
        raise IOError("directory '{}' not found.".format(main_dir))

    sub_dirs = [os.path.join(main_dir, d) for d in os.listdir(main_dir)]

    idx = 0
    while idx < len(sub_dirs):
        batch = [get_snapshot_data_from_sub_dir(sub_dir, frame_skip)
                     for sub_dir in sub_dirs[idx:idx+batch_size]]

        snap_data_batch, sub_dir_batch = split_snapshot_data_and_subdir_names(batch)
        idx = 0 if index_needs_reset() else idx + batch_size
        snapshot_batch = make_snapshots_from_snapshot_data(snap_data_batch)

        if index_needs_reset() and shuffle:
            np.random.shuffle(sub_dirs)

        if return_dict:
            yield group_snapshots_by_dir_name(sub_dir_batch, snapshot_batch)
        else:
            yield list(snapshot_batch)


def get_snapshot_data_from_sub_dir(sub_dir, frame_skip=0):
    sub_dir_name = os.path.split(sub_dir)[1]
    yaml_file = os.path.join(sub_dir, sub_dir_name + ".yaml")
    yaml = YAML()
    snapshot_data_list = []

    with open(yaml_file) as data_file:
        yaml_content = yaml.load(data_file)

    for counter, yaml_entry in enumerate(yaml_content):
        if (counter + 1) % (frame_skip + 1) == 0:
            actions, timestamp, abs_img_path = convert_config_entry_to_snapshot_data(entry=yaml_entry,
                                                                                     image_dir=sub_dir)
            snapshot_data_list.append((sub_dir_name, actions, timestamp, abs_img_path))

    return snapshot_data_list


def convert_config_entry_to_snapshot_data(entry, image_dir):
    timestamp = entry.get("timestamp")
    rel_img_path = entry.get("image")
    abs_img_path = os.path.join(image_dir, rel_img_path)
    actions = entry.get("actions")

    try:
        timestamp = int(timestamp)
    except ValueError:
        raise ValueError("couldn't cast timestamp '{}' to in".format(timestamp))

    if not os.path.isfile(abs_img_path):
        raise IOError(
            "Image '{}' not found, but listed in config".format(abs_img_path))

    return actions, timestamp, abs_img_path


def make_snapshot_from_data(snapshot_data):
    actions, timestamp, img_path = snapshot_data
    image_data = cv2.imread(img_path)
    _, compressed_image = cv2.imencode(IMAGE_FILE_EXTENSION, image_data)
    return ImageSnapshot(actions, timestamp, compressed_image)
