from multiprocessing.pool import ThreadPool

import numpy as np
from tqdm import tqdm

from neuroracer_ai.processors import ImageProcessor
from neuroracer_ai.suites import AbstractProcessorSuite
from neuroracer_ai.utils.config_handler import NB_CPU

PROGRESS_DESC = "Processing 2D Images"


class Image2DProcessorSuite(AbstractProcessorSuite):
    """
    A ProcessorSuite which processes images with an internal ImageProcessor instance.
    parameters for the processor can be set in the constructor.

    It requires the data to be iterable!
    """

    def __init__(self, image_processor_params=None, multithreading_chunksize=1000):
        AbstractProcessorSuite.__init__(self)
        self._image_processor = ImageProcessor(params=image_processor_params)
        self._chunksize = multithreading_chunksize

    def process(self, data, use_multithreading=False):
        images = data
        processed_images = None
        if images is not None:
            # creation of the numpy array for all uncompressed images (first
            # images is processed without a loop to get the needed information
            # for the shape)
            processed_img = self._image_processor.process(images[0])
            list_shape = (len(images),) + processed_img.shape
            processed_images = np.empty(list_shape, dtype=processed_img.dtype)

            # we are preventing a double processing by removing the first value
            # from the list
            processed_images[0] = processed_img
            unprocessed_images = images[1:]
            if use_multithreading:
                pool = ThreadPool(processes=NB_CPU)
                result_iterator = pool.imap(
                    func=self._image_processor.process,
                    iterable=unprocessed_images,
                    chunksize=self._chunksize)
                for index, img in enumerate(result_iterator, 1):
                    processed_images[index] = img
                pool.close()
                pool.join()
            else:
                progress_bar_iterator = tqdm(
                    iterable=unprocessed_images,
                    desc=PROGRESS_DESC,
                    total=len(unprocessed_images))
                for i, processed_img in enumerate(unprocessed_images, 1):
                    processed_images[i] = self._image_processor.process(processed_img)

        return processed_images
