"""
Contains AbstractProcessorSuite implementations. A ProcessorSuite describes how
a input gets processed before giving it to the ai model. This package contains
several predefined ProcessorSuites which are already ready to go. If required it
is also possible to configure your own through the ManualMappingProcessorSuite.

A user should never implement its own AbstractProcessorSuite! As this causes
problems with the saving of the ai.
"""

from neuroracer_ai.suites.abstract import AbstractProcessorSuite
from neuroracer_ai.suites.image import Image2DProcessorSuite
from neuroracer_ai.suites.general import EmptyProcessorSuite, ManualMappingProcessorSuite

__all__ = [
    "AbstractProcessorSuite",
    "Image2DProcessorSuite",
    "EmptyProcessorSuite",
    "ManualMappingProcessorSuite"
]
