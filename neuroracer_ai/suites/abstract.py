from abc import ABCMeta, abstractmethod


class AbstractProcessorSuite:
    """
    Abstract ProcessorSuite base class.

    Derived class should only exist in the library! As it otherwise breaks the
    saving of the ai!
    """

    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    @abstractmethod
    def process(self, data, use_multithreading=False):
        """
        Processes the data and returns the processed data. If needed you can
        also add a multiprocessing.pool.Pool object which will be used to
        multiprocess the processing.

        :param data: data to process
        :type data: Union[Dict[str, np.ndarray], np.ndarray]
        :return: processed data
        :type use_multithreading: bool
        :rtype: Union[Dict[str, np.ndarray], np.ndarray]
        """
        pass

