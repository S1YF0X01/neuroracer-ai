#from typing import Dict

from neuroracer_ai.suites import AbstractProcessorSuite
from neuroracer_ai.utils.shared_value_dict import SharedValueDict


class EmptyProcessorSuite(AbstractProcessorSuite):
    """
    A simple ProcessorSuite which just returns the given data without doing
    anything with it. If you don't need any processing.
    """

    def process(self, data):
        """
        Returns the given data without modifying it.

        :param data: data to process
        :type data: Union[Dict[str, np.ndarray], np.ndarray]
        :return: processed data
        :rtype: Union[Dict[str, np.ndarray], np.ndarray]
        """

        return data


class ManualMappingProcessorSuite(AbstractProcessorSuite):
    """
    A customizable ProcessorSuite which can be configured through a mapping
    dictionary. It only works if data is a dictionary.
    """

    def __init__(self, mapping):
        """
        :param mapping: mapping dictionary with the same keys as the data dict
                which gets later and AbstractProcessor implementations as values
        :type mapping:  Dict[str, neuroracer_ai.processors.AbstractProcessor]
        """

        super(ManualMappingProcessorSuite, self).__init__()
        self._mapping = SharedValueDict(mapping)

    def process(self, data, use_multithreading=False):
        """
        Processes the data and returns the processed data.

        :param data: data to process
        :type data: Union[Dict[str, np.ndarray], np.ndarray]
        :return: processed data
        :rtype: Union[Dict[str, np.ndarray], np.ndarray]
        :param use_multithreading: atm not supported (usage of it is ignored)
        :type use_multithreading: bool
        :raises TypeError: if the parameter is not a dict
        """

        if data is not dict:
            TypeError("data parameter must be of type dict!")

        processed_data = data.copy()

        for input_key, processor_data in processed_data.items():
            if input_key in self._mapping:
                processed_data[input_key] = self._mapping[input_key].process(processor_data)

        return processed_data
