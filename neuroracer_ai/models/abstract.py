from abc import ABCMeta, abstractmethod


class AbstractModel:
    """
    Abstract Model interface. It abstracts the raw Keras interface from the
    user as well as the rest of the lib.
    """

    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    @staticmethod
    @abstractmethod
    def load(model_path):
        """
        Tries to load the given file and returns a new instance of this object
        if it is successful.

        :param model_path:
        :type model_path: str
        :return: created Model instance
        :rtype: neuroracer_ai.models.AbstractModel
        :raises IOError: if loading of the model fails
        """

        pass

    @staticmethod
    @abstractmethod
    def create(architecture_func, architecture_func_params):
        """
        Creates a new model based on the given architecture_func and its params.

        :param architecture_func: architecture construction function
        :type architecture_func: Callable[[**kwargs], keras.Model]
        :param architecture_func_params: parameters which get passed to to the
                architecture construction function
        :type architecture_func_params: Dict[str, Any]
        :return: created Model instance
        :rtype: neuroracer_ai.models.AbstractModel
        """

        pass

    @abstractmethod
    def predict(self, s, verbose=0):
        """
        Predicts output based on the internal nn model.

        :param s: TODO
        :type s: numpy.ndarray
        :param verbose: sets the verbosity level of the model 0 = silent,
                1 = progress bar, 2 line per epoch
        :type verbose: int
        :return: prediction of the network.
        :rtype: numpy.ndarray
        """

        pass

    @abstractmethod
    def train(self, params, checkpoint_path, verbose=0):
        """
        Trains the model with the given data and returns the history of it.

        :param params: train x data
        :type params: ai.TrainParameters
        :param checkpoint_path: dir + file name for the model
        :type checkpoint_path: str
        :param verbose: sets the verbosity level of the model 0 = silent,
                1 = progress bar, 2 line per epoch
        :type verbose: int
        :return: History object of the training
        :rtype: Dict
        """

        pass

    @abstractmethod
    def save(self, path, overwrite=True):
        """
        Saves the model at the given path.

        :param path: save path (consists of dir + file)
        :type path: str
        :param overwrite: manages if model files should be overwritten
        :type overwrite: bool
        """

        pass


class Debuggable:
    """
    Defines if a model is debuggable of visualizing itself.
    """

    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    @abstractmethod
    def visualize_activation(self, layer_name=None, layer_idx=None, filter_indices=None, seed_input=None):
        """
        Visualizes the activation of the nn and returns it as a numpy array.

        :param layer_name: name of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_name: str
        :param layer_idx: index of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_idx: int
        :param filter_indices: list of indices from filters which get
                visualized. if none all are visualized
        :type filter_indices: List[int]
        :param seed_input: list of images which help the visualization to
                converge
        :type seed_input: np.ndarray
        :return: visualization
        :rtype: np.ndarray
        :raises TypeError: if the backend doesn't support visualization
        """

        pass

    @abstractmethod
    def visualize_saliency(self, layer_name=None, layer_idx=None, filter_indices=None, seed_input=None):
        """
        Visualizes the saliency of the nn and returns it as a numpy array.

        :param layer_name: name of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_name: str
        :param layer_idx: index of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_idx: int
        :param filter_indices: list of indices from filters which get
                visualized. if none all are visualized
        :type filter_indices: List[int]
        :param seed_input: list of images which help the visualization to
                converge
        :type seed_input: np.ndarray
        :return: visualization
        :rtype: np.ndarray
        :raises TypeError: if the backend doesn't support visualization
        """

        pass

    @abstractmethod
    def get_weights(self, layer_name=None, layer_idx=None):
        """
        Retrieves the weights from a certain layer.

        :param layer_name: name of the layer. if both layer_idx and layer_name
                are given layer_name has priority.
        :param layer_idx: index of the layer
        :return: weights of the layer
        :rtype: np.ndarray
        """

        pass

    @abstractmethod
    def get_summary(self):
        """
        Constructs the summary of the structure and returns it as a string.

        :return: summary string of the model
        :rtype: str
        """

        pass
