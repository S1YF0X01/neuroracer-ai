"""
Everything which belongs to the keras backend.
"""

import os
import warnings

# hide keras future warnings as we can't change them and they bleed into our output
with warnings.catch_warnings():
    warnings.simplefilter(action="ignore", category=FutureWarning)

    import keras as k
    import keras.initializers as ki
    import keras.layers as kl
    import keras.models as km
    import keras.optimizers as opt
    import vis.utils.utils as kvu
    import vis.visualization as kv

from neuroracer_ai.models import AbstractModel, Debuggable


# TODO make switch for de-/activation
# workaround to get models running on the board
# import tensorflow as tf
# from keras.backend.tensorflow_backend import set_session
#
# config = tf.ConfigProto()
# config.gpu_options.allow_growth = False
# config.gpu_options.per_process_gpu_memory_fraction = 0.3
# set_session(tf.Session(config=config))


class KerasModel(AbstractModel, Debuggable):
    """
    Keras based AbstractModel implementation.
    """

    def __init__(self):
        super(KerasModel, self).__init__()
        self._internal_model = None  # type: km.Model

    @staticmethod
    def load(model_path, trainable=True):
        """
        Tries to load the given file and returns a new instance of this object
        if it is successful. Trainable defines if you can train a model after
        loading. If you try to train an untrainable model an Error will be
        thrown.

        :param model_path:
        :type model_path: str
        :param trainable: loads the model in a way which allows it to be trained.
        :type trainable: bool
        :return created Model instance
        :rtype: keras.KerasModel
        :raises IOError: if loading of the model fails
        """


        try:
            # TODO
            # maybe use custom_objects to provide custom_metrics
            model = k.models.load_model(os.path.expanduser(model_path),
                                        compile=trainable)

            new_model = KerasModel()
            new_model._internal_model = model
            return new_model
        except ValueError as e:
            raise IOError("Trying to load the model ({}) failed!\nMessage: {}".format(model_path, e))

    @staticmethod
    def create(architecture_func, architecture_func_params, print_summary=True):
        """
        Creates a new model based on the given architecture_func and its params.

        :param architecture_func: architecture construction function
        :type architecture_func: Callable[[**kwargs], keras.Model]
        :param architecture_func_params: parameters which get passed to to the
                architecture construction function
        :type architecture_func_params: Dict[str, Any]
        :param print_summary: defines if the summary of the model gets printed at the end
        :type print_summary: bool
        :return: created Model instance
        :rtype: KerasModel
        """

        # todo: should we catch exceptions here?
        new_model = KerasModel()
        new_model._internal_model = architecture_func(**architecture_func_params)

        if print_summary:
            new_model._internal_model.summary()

        return new_model

    def predict(self, s, verbose=0):
        """
        Predicts output based on the model.

        :param s: TODO
        :type s: numpy.ndarray
        :param verbose: sets the verbosity level of the model 0 = silent,
                1 = progress bar, 2 line per epoch
        :type verbose: int
        :return:
        :rtype: numpy.ndarray
        """

        return self._internal_model.predict(s, verbose=verbose)

    def train(self, params, checkpoint_path, verbose=0):
        """
        Trains the model with the given data and returns the history of it.

        :param params: train x data
        :type params: ai.TrainParameters
        :param checkpoint_path: dir + file name for the model
        :type checkpoint_path: str
        :param verbose: sets the verbosity level of the model 0 = silent,
                1 = progress bar, 2 line per epoch
        :type verbose: int
        :return: History object of the training
        :rtype: Dict
        """

        callbacks = []
        if params.use_checkpoints:
            callbacks.append(
                k.callbacks.ModelCheckpoint(
                    filepath=os.path.expanduser(checkpoint_path),
                    save_best_only=True,
                    save_weights_only=False,
                    mode="min"))
        # TODO move hardcoded params to TrainParams Class
        history = self._internal_model.fit_generator(
                                           generator=params.train_data_generator,
                                           validation_data=params.validation_data_generator,
                                           validation_steps=5,
                                           steps_per_epoch=10,
                                           epochs=params.epochs,
                                           shuffle=True,
                                           callbacks=callbacks,
                                           verbose=verbose)

        return history

    def save(self, path, overwrite=True):
        """
        Saves the model at the given path.

        :param path: save path (consists of dir + file)
        :type path: str
        :param overwrite: manages if model files should be overwritten
        :type overwrite: bool
        """

        self._internal_model.save(filepath=os.path.expanduser(path), overwrite=overwrite)

    def _resolve_layer_idx(self, layer_name, layer_idx):
        idx = layer_idx
        if layer_name:
            idx = kvu.find_layer_idx(self._internal_model, layer_name)

        return idx

    def visualize_activation(self, layer_name=None, layer_idx=None, filter_indices=None, seed_input=None):
        idx = self._resolve_layer_idx(layer_name=layer_name, layer_idx=layer_idx)

        return kv.visualize_activation(
            model=self._internal_model,
            layer_idx=idx,
            filter_indices=filter_indices,
            seed_input=seed_input,
            backprop_modifier="guided")

    def visualize_saliency(self, layer_name=None, layer_idx=None, filter_indices=None, seed_input=None):
        idx = self._resolve_layer_idx(layer_name=layer_name, layer_idx=layer_idx)

        return kv.visualize_saliency(
            model=self._internal_model,
            layer_idx=idx,
            filter_indices=filter_indices,
            seed_input=seed_input,
            backprop_modifier="guided")

    def get_weights(self, layer_name=None, layer_idx=None):
        layer = self._internal_model.get_layer(name=layer_name, index=layer_idx)

        if layer is None:
            return None

        return layer.get_weights()

    def get_summary(self):
        summary_list = []
        def append_lines(line):
            if len(summary_list) != 0:
                summary_list.append("\n")
            summary_list.append(line)

        self._internal_model.summary(print_fn=append_lines)
        return "".join(summary_list)

class KerasArchitectures:
    """
    Available architectures for keras models.
    """

    def __init__(self):
        pass

    @staticmethod
    def htw_modified_nvidia(image_shape):
        """
        This architecture is a modified nvidia approach by the HTW Berlin.

        :param image_shape: TODO
        :type image_shape: Tuple[int, int, int]
        :return: the constructed model instance
        :rtype: keras.models.Model
        """

        s = kl.Input(shape=image_shape, name="observation_input")

        c1 = kl.Conv2D(activation="relu", filters=8, kernel_size=(5, 5), padding="valid", name="conv1",
                       kernel_initializer=ki.VarianceScaling(mode="fan_in", distribution="uniform"))(s)
        c2 = kl.Conv2D(filters=12, kernel_size=(5, 5), activation="relu", name="conv2",
                       kernel_initializer=ki.VarianceScaling(mode="fan_in", distribution="uniform"))(c1)
        c3 = kl.Conv2D(filters=16, kernel_size=(5, 5), activation="relu", name="conv3",
                       kernel_initializer=ki.VarianceScaling(mode="fan_in", distribution="uniform"))(c2)
        c4 = kl.Conv2D(filters=24, kernel_size=(3, 3), activation="relu", name="conv4",
                       kernel_initializer=ki.VarianceScaling(mode="fan_in", distribution="uniform"))(c3)
        c5 = kl.Conv2D(filters=24, kernel_size=(3, 3), activation="relu", name="conv5",
                       kernel_initializer=ki.VarianceScaling(mode="fan_in", distribution="uniform"))(c4)

        observation_flattened = kl.Flatten(name="flattened_observation")(c5)

        d1 = kl.Dense(units=1164, activation="relu", name="fully_con1",
                      kernel_initializer="glorot_uniform")(observation_flattened)
        d1_drop = kl.Dropout(0.2)(d1)
        d2 = kl.Dense(units=100, activation="relu", name="fully_con2",
                      kernel_initializer="glorot_uniform")(d1_drop)
        d2_drop = kl.Dropout(0.2)(d2)
        d3 = kl.Dense(units=50, activation="relu", name="fully_con3",
                      kernel_initializer="glorot_uniform")(d2_drop)
        d3_drop = kl.Dropout(0.2)(d3)
        d4 = kl.Dense(units=10, activation="tanh", name="fully_con4",
                      kernel_initializer="glorot_uniform")(d3_drop)
        d4_drop = kl.Dropout(0.2)(d4)

        steer_output = kl.Dense(units=1,
                                activation="tanh",
                                name="prediction_steer",
                                bias_initializer="zeros",
                                kernel_initializer=ki.RandomUniform(minval=-3e-4, maxval=3e-4)
                                )(d4_drop)
        speed_output = kl.Dense(units=1,
                                activation="sigmoid",
                                name="prediction_speed",
                                bias_initializer="zeros",
                                kernel_initializer=ki.RandomUniform(minval=0.0, maxval=3e-4)  # minval should
                                # be -3e-4 bu we only need forward driving at the moment
                                )(d4_drop)

        merged_output = kl.concatenate([steer_output, speed_output], name="merge_concatenate_predictions")

        model = km.Model(inputs=s, outputs=merged_output)
        model.compile(loss="mse", optimizer="adam", metrics=["mse"])

        return model

    @staticmethod
    def nvidia_functional(image_shape):
        """
        The architecture which has been used in the End to End Learning for
        Self-Driving Cars paper (https://arxiv.org/abs/1604.07316) from nvidia.

        :param image_shape: TODO
        :type image_shape: Tuple[int, int, int]
        :return: the constructed model instance
        :rtype: keras.models.Model
        """

        s = kl.Input(shape=image_shape, name="observation_input")

        c1 = kl.Conv2D(filters=24, kernel_size=(5, 5), strides=(2, 2), bias_initializer="he_normal", padding="valid",
                       activation="relu")(s)
        c2 = kl.Conv2D(filters=36, kernel_size=(5, 5), strides=(2, 2), bias_initializer="he_normal", padding="valid",
                       activation="relu")(c1)
        c3 = kl.Conv2D(filters=48, kernel_size=(5, 5), strides=(2, 2), bias_initializer="he_normal", padding="valid",
                       activation="relu")(c2)
        c4 = kl.Conv2D(filters=64, kernel_size=(3, 3), bias_initializer="he_normal", padding="valid",
                       activation="relu")(c3)
        c5 = kl.Conv2D(filters=64, kernel_size=(3, 3), bias_initializer="he_normal", padding="valid",
                       activation="relu")(c4)

        observation_flattened = kl.Flatten(name="flattened_observation")(c5)

        d1 = kl.Dense(units=1164, activation="relu", name="fully_con1",
                      bias_initializer="he_normal")(observation_flattened)
        d1_drop = kl.Dropout(0.2)(d1)
        d2 = kl.Dense(units=100, activation="relu", name="fully_con2",
                      bias_initializer="he_normal")(d1_drop)
        d2_drop = kl.Dropout(0.2)(d2)
        d3 = kl.Dense(units=50, activation="relu", name="fully_con3",
                      bias_initializer="he_normal")(d2_drop)
        d3_drop = kl.Dropout(0.2)(d3)
        d4 = kl.Dense(units=10, activation="relu", name="fully_con4",
                      bias_initializer="he_normal")(d3_drop)
        d4_drop = kl.Dropout(0.2)(d4)

        steer_output = kl.Dense(units=1,
                                activation="tanh",
                                name="prediction_steer",
                                bias_initializer="he_normal",
                                )(d4_drop)
        speed_output = kl.Dense(units=1,
                                activation="sigmoid",
                                name="prediction_speed",
                                bias_initializer="he_normal",
                                )(d4_drop)

        merged_output = kl.concatenate([steer_output, speed_output], name="merge_concatenate_predictions")

        model = km.Model(inputs=s, outputs=merged_output)
        model.compile(loss="mse", optimizer="adam", metrics=["mse"])

        return model

    @staticmethod
    def nvidia(image_shape):
        """
        The architecture which has been used in the End to End Learning for
        Self-Driving Cars paper (https://arxiv.org/abs/1604.07316) from nvidia.

        :param image_shape: TODO
        :type image_shape: Tuple[int, int, int]
        :return: the constructed model instance
        :rtype: keras.models.Model
        """

        model = km.Sequential()

        # Convolution no.1
        model.add(
            kl.Conv2D(filters=24, kernel_size=(5, 5), strides=(2, 2), bias_initializer="he_normal", padding="valid",
                      input_shape=image_shape, activation="relu"))

        # Convolution no.2
        model.add(
            kl.Conv2D(filters=36, kernel_size=(5, 5), strides=(2, 2), bias_initializer="he_normal", padding="valid",
                      activation="relu"))

        # Convolution no.3
        model.add(
            kl.Conv2D(filters=48, kernel_size=(5, 5), strides=(2, 2), bias_initializer="he_normal", padding="valid",
                      activation="relu"))

        # Convolution no.4
        model.add(
            kl.Conv2D(filters=64, kernel_size=(3, 3), bias_initializer="he_normal", padding="valid", activation="relu"))

        # Convolution no.5
        model.add(
            kl.Conv2D(filters=64, kernel_size=(3, 3), bias_initializer="he_normal", padding="valid", activation="relu"))
        model.add(kl.Flatten())

        # Fully connected no.1
        model.add(kl.Dense(units=1164, bias_initializer="he_normal", activation="relu"))
        model.add(kl.Dropout(0.2))

        # Fully connected no.2
        model.add(kl.Dense(units=100, bias_initializer="he_normal", activation="relu"))
        model.add(kl.Dropout(0.2))

        # Fully connected no.3
        model.add(kl.Dense(units=50, bias_initializer="he_normal", activation="relu"))

        # Fully connected no.4
        model.add(kl.Dense(units=10, bias_initializer="he_normal", activation="tanh"))

        # Fully connected no.5
        model.add(kl.Dense(units=1, bias_initializer="he_normal", activation="tanh", name="prediction"))

        model.compile(loss="mse", optimizer="adam", metrics=["mse"])

        return model

    @staticmethod
    def lillicrap(image_shape):
        """
        Approach of Reinforcement Learning DDPG algorithm (actor)
        https://arxiv.org/abs/1509.02971

        :param image_shape: TODO
        :type image_shape: Tuple[int, int, int]
        :return: the constructed model instance
        :rtype: keras.models.Model
        """

        s = kl.Input(shape=image_shape, name="observation_input")

        c1 = kl.Conv2D(filters=32, kernel_size=(4, 4), activation="relu", name="conv1", padding="valid",
                       kernel_initializer=ki.VarianceScaling(mode="fan_in", distribution="uniform"))(s)
        c2 = kl.Conv2D(filters=32, kernel_size=(4, 4), activation="relu", name="conv2",
                       kernel_initializer=ki.VarianceScaling(mode="fan_in", distribution="uniform"))(c1)
        c3 = kl.Conv2D(filters=32, kernel_size=(4, 4), activation="relu", name="conv3",
                       kernel_initializer=ki.VarianceScaling(mode="fan_in", distribution="uniform"))(c2)
        c3_flatten = kl.Flatten(name="flattened_observation")(c3)

        d1 = kl.Dense(units=200, activation="relu", kernel_initializer="glorot_uniform", name="fully_con1")(c3_flatten)
        d2 = kl.Dense(units=200, activation="tanh", kernel_initializer="glorot_uniform", name="fully_con2")(d1)

        steer_output = kl.Dense(units=1,
                                activation="tanh",
                                name="prediction_steer",
                                bias_initializer="zeros",
                                kernel_initializer=ki.RandomUniform(minval=-3e-4, maxval=3e-4)
                                )(d2)
        speed_output = kl.Dense(units=1,
                                activation="sigmoid",
                                name="prediction_speed",
                                bias_initializer="zeros",
                                kernel_initializer=ki.RandomUniform(minval=0.0, maxval=3e-4)
                                )(d2)

        merged_output = kl.concatenate([steer_output, speed_output], name="merge_concatenate_predictions")

        model = km.Model(inputs=s, outputs=merged_output)

        model.compile(loss="mse", optimizer="adam", metrics=["mse"])

        return model

    @staticmethod
    def small_net(image_shape):
        """
        Self driving approach with a small network

        :param image_shape: TODO
        :type image_shape: Tuple[int, int, int]
        :return: the constructed model instance
        :rtype: keras.models.Model
        """

        model = km.Sequential()

        model.add(kl.Conv2D(
            filters=8,
            kernel_size=(7, 7),
            activation="relu",
            name="conv1",
            kernel_initializer="glorot_uniform",
            input_shape=image_shape,
            strides=2,
            kernel_regularizer=k.regularizers.l1(0.008)))

        model.add(kl.BatchNormalization())

        model.add(kl.Conv2D(
            filters=16,
            kernel_size=(5, 5),
            activation="relu",
            name="conv2",
            kernel_initializer="glorot_uniform",
            kernel_regularizer=k.regularizers.l1(0.008)))

        model.add(kl.BatchNormalization())

        model.add(kl.Conv2D(
            filters=16,
            kernel_size=(5, 5),
            activation="relu",
            name="conv3",
            kernel_initializer="glorot_uniform",
            kernel_regularizer=k.regularizers.l1(0.002)))

        model.add(kl.Flatten(name="flattened_observation"))

        model.add(kl.Dense(
            units=150, activation="relu", kernel_initializer="glorot_uniform"))

        model.add(kl.Dropout(0.2))

        model.add(kl.Dense(
            units=80, activation="relu", kernel_initializer="glorot_uniform"))

        model.add(kl.Dense(
            units=1, kernel_initializer="glorot_uniform", name="prediction"))

        model.compile(loss="mse", optimizer="adam", metrics=["mse"])

        return model


    @staticmethod
    def small_net2(image_shape):
        """
        Self driving approach with a small network

        :param image_shape: TODO
        :type image_shape: Tuple[int, int, int]
        :return: the constructed model instance
        :rtype: keras.models.Model
        """

        model = km.Sequential()

        model.add(kl.Conv2D(
            filters=8,
            kernel_size=(7, 7),
            activation="relu",
            name="conv1",
            kernel_initializer="glorot_uniform",
            input_shape=image_shape,
            strides=2,
            kernel_regularizer=k.regularizers.l1(0.008)))

        model.add(kl.BatchNormalization())

        model.add(kl.Conv2D(
            filters=16,
            kernel_size=(5, 5),
            activation="relu",
            name="conv2",
            kernel_initializer="glorot_uniform",
            kernel_regularizer=k.regularizers.l1(0.008)))

        model.add(kl.BatchNormalization())

        model.add(kl.Conv2D(
            filters=16,
            kernel_size=(5, 5),
            activation="relu",
            name="conv3",
            kernel_initializer="glorot_uniform",
            kernel_regularizer=k.regularizers.l1(0.002)))

        model.add(kl.Flatten(name="flattened_observation"))

        model.add(kl.Dense(
            units=250, activation="relu", kernel_initializer="glorot_uniform"))

        model.add(kl.Dropout(0.2))

        model.add(kl.Dense(
            units=120, activation="relu", kernel_initializer="glorot_uniform"))

        model.add(kl.Dense(
            units=1, kernel_initializer="glorot_uniform", name="prediction"))

        model.compile(loss="mse", optimizer="adam", metrics=["mse"])

        return model


    @staticmethod
    def lecun_dave(image_shape):
        """
        Originally intended for obstacle avoidance but it is a much harder task, so could work for pylon following
        http://yann.lecun.com/exdb/publis/pdf/lecun-dave-05.pdf
        :param image_shape:
        :return:
        """

        s = kl.Input(shape=image_shape, name="observation_input")

        c1 = kl.Conv2D(filters=6, kernel_size=(3, 3), activation="relu", name="conv1", padding="valid",
                       kernel_initializer=ki.VarianceScaling(mode="fan_in", distribution="uniform"))(s)
        c2 = kl.Conv2D(filters=32, kernel_size=(4, 4), activation="relu", name="conv2",
                       kernel_initializer=ki.VarianceScaling(mode="fan_in", distribution="uniform"))(c1)
        c3 = kl.Conv2D(filters=32, kernel_size=(4, 4), activation="relu", name="conv3",
                       kernel_initializer=ki.VarianceScaling(mode="fan_in", distribution="uniform"))(c2)
        c3_flatten = kl.Flatten(name="flattened_observation")(c3)

        d1 = kl.Dense(units=200, activation="relu", kernel_initializer="glorot_uniform", name="fully_con1")(c3_flatten)
        d2 = kl.Dense(units=200, activation="tanh", kernel_initializer="glorot_uniform", name="fully_con2")(d1)

        steer_output = kl.Dense(units=1,
                                activation="tanh",
                                name="prediction_steer",
                                bias_initializer="zeros",
                                kernel_initializer=ki.RandomUniform(minval=-3e-4, maxval=3e-4)
                                )(d2)
        speed_output = kl.Dense(units=1,
                                activation="sigmoid",
                                name="prediction_speed",
                                bias_initializer="zeros",
                                kernel_initializer=ki.RandomUniform(minval=0.0, maxval=3e-4)
                                )(d2)

        merged_output = kl.concatenate([steer_output, speed_output], name="merge_concatenate_predictions")

        model = km.Model(inputs=s, outputs=merged_output)

        return model

    @staticmethod
    def onpu_modified_nvidia(image_shape):
        """
        This architecture is a modified nvidia approach by the Odessa National Polytechnic University.

        :param image_shape: TODO
        :type image_shape: Tuple[int, int, int]
        :return: the constructed model instance
        :rtype: keras.models.Model
        """

        model = km.Sequential()

        model.add(kl.BatchNormalization(input_shape=image_shape))

        # Convolution no.1
        model.add(
            kl.Conv2D(
                filters=4,
                kernel_size=(5, 5),
                strides=(2, 2),
                bias_initializer='he_normal',
                padding="valid"))
        model.add(kl.ELU())

        model.add(kl.MaxPooling2D(pool_size=(2, 2)))
        model.add(kl.BatchNormalization())

        # Convolution no.2
        model.add(
            kl.Conv2D(
                filters=6,
                kernel_size=(5, 5),
                strides=(2, 2),
                bias_initializer='he_normal',
                padding="valid"))
        model.add(kl.ELU())

        model.add(kl.BatchNormalization())

        # Convolution no.3
        model.add(
            kl.Conv2D(
                filters=8,
                kernel_size=(3, 3),
                bias_initializer='he_normal',
                padding="valid"))
        model.add(kl.ELU())
        model.add(kl.BatchNormalization())

        model.add(kl.Flatten())

        # Fully connected no.1
        model.add(
            kl.Dense(
                units=75,
                bias_initializer='he_normal'))
        model.add(kl.ELU())
        model.add(kl.BatchNormalization())
        model.add(kl.Dropout(0.25))

        # Fully connected no.2
        model.add(
            kl.Dense(
                units=25,
                bias_initializer='he_normal'))
        model.add(kl.ELU())
        model.add(kl.BatchNormalization())
        model.add(kl.Dropout(0.25))

        # Fully connected no.3
        model.add(
            kl.Dense(
                units=1,
                bias_initializer='he_normal',
                activation='tanh',
                name='prediction'))

        optimizer = opt.Nadam()
        model.compile(loss='mse', optimizer=optimizer, metrics=['mse'])

        return model

    @staticmethod
    def test_net(image_shape):
        """
        Custom self driving approach based on the architecture of the
        polytechnic university of odessa.

        :param image_shape: TODO
        :type image_shape: Tuple[int, int, int]
        :return: the constructed model instance
        :rtype: keras.models.Model
        """

        model = km.Sequential()

        model.add(kl.Conv2D(
            filters=4, kernel_size=(5, 5), activation="relu", name="conv1",
            kernel_initializer="glorot_uniform",
            input_shape=image_shape, kernel_regularizer=k.regularizers.l1_l2(0.005, 0.001)))
        model.add(kl.BatchNormalization())

        model.add(kl.Conv2D(
            filters=4, kernel_size=(5, 5), activation="relu", name="conv2",
            kernel_initializer="glorot_uniform",
            input_shape=image_shape, kernel_regularizer=k.regularizers.l1_l2(0.005, 0.001)))
        model.add(kl.BatchNormalization())

        model.add(kl.Flatten(name="flattened_observation"))

        model.add(kl.Dense(
            units=120, activation="relu", kernel_initializer="glorot_uniform"))
        model.add(kl.Dropout(0.25))

        model.add(kl.Dense(
            units=1, kernel_initializer="glorot_uniform", name="prediction"))

        model.compile(loss="mse", optimizer="adam", metrics=["mse"])

        return model

    @staticmethod
    def mirror_net(image_shape):
        """
        Custom self driving approach based on the architecture of the
        polytechnic university of odessa.

        :param image_shape: TODO
        :type image_shape: Tuple[int, int, int]
        :return: the constructed model instance
        :rtype: keras.models.Model
        """

        model = km.Sequential()

        model.add(kl.Conv2D(
            filters=4, kernel_size=(5, 5), activation="relu", name="conv1",
            kernel_initializer="glorot_uniform",
            input_shape=image_shape))
        model.add(kl.BatchNormalization())

        model.add(kl.Conv2D(
            filters=4, kernel_size=(5, 5), activation="relu", name="conv2",
            kernel_initializer="glorot_uniform",
            input_shape=image_shape))
        model.add(kl.BatchNormalization())

        model.add(kl.Flatten(name="flattened_observation"))

        model.add(kl.Dense(
            units=150, activation="relu", kernel_initializer="glorot_uniform"))

        model.add(kl.Dense(
            units=1, kernel_initializer="glorot_uniform", name="prediction"))

        model.compile(loss="mse", optimizer="adam", metrics=["mse"])

        return model
