"""
Contains the AbstractModel interface and all implementations of it. Available
architectures for the models are also in here. This servers as a abstraction
from the used framework.
"""

from neuroracer_ai.models.abstract import AbstractModel, Debuggable
from neuroracer_ai.models.keras_backend import KerasModel, KerasArchitectures

__all__ = [
    "AbstractModel",
    "Debuggable",
    "KerasModel",
    "KerasArchitectures"
]