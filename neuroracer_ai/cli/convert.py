#!/usr/bin/env python

import argparse

from pymp import Parallel

import neuroracer_ai.utils.bag_reader as bag_reader
import neuroracer_ai.utils.config_handler as config_handler
from neuroracer_ai.utils.arg_handler import get_argparser
from neuroracer_ai.utils.data_handler import write_snapshot_iterable


def main():
    """
    Converts specified sources into faster readable data.
    """

    args = _get_arguments()

    # set default values. If no configfile is provided and no actionline
    # arguments are parsed, bag_reader will use default values for the topics.
    config = {"camera_topic": None, "action_topic": None, "frame_skip": 0}

    if args.configfile:
        config = config_handler.read_config_file(args.configfile)

    # override config file configurations with action line arguments
    if args.sources:
        config["sources"] = args.sources
    if args.destination:
        config["destination"] = args.destination
    if args.camera_topic:
        config["camera_topic"] = args.camera_topic
    if args.action_topic:
        config["action_topic"] = args.action_topic
    if args.frame_skip:
        config["frame_skip"] = args.frame_skip
    if args.verbose:
        config["verbose"] = args.verbose
    if args.write_as_archive:
        config["write_as_archive"] = args.write_as_archive

    verbose = 1
    if "verbose" in config:
        verbose = config["verbose"]

    config_handler.check_config(config, and_required_keys=["sources", "destination",
                                                           "camera_topic", "action_topic",
                                                           "frame_skip"])

    rosbag_list = bag_reader.get_rosbag_list(config["sources"])

    if verbose > 0:
        print("Found {} rosbags".format(str(len(rosbag_list))))

    rosbag_dict_generator = bag_reader.read_rosbags_as_generator(
                                                          bag_sources=config["sources"],
                                                          camera_topic=config["camera_topic"],
                                                          action_topic=config["action_topic"],
                                                          frame_skip=config["frame_skip"],
                                                          nb_cpu=config_handler.NB_CPU,
                                                          batch_size=12,
                                                          return_dict=True,
                                                          loop_indefinitely=False)

    # write rosbags into destination
    with Parallel(config_handler.NB_CPU) as p:
        for i, rosbag_dict in enumerate(rosbag_dict_generator):
            print "working on batch %s" % i
            for key in p.iterate(rosbag_dict.keys()):
                container_name = bag_reader.get_container_name(key)
                with p.lock:
                    snapshot_list = rosbag_dict[key]

                write_snapshot_iterable(
                    config["destination"],
                    container_name,
                    snapshot_list,
                    verbose=verbose,
                    as_archive=config["write_as_archive"])

                del snapshot_list


def _get_arguments():
    """
    Creates the ArgumentParser and returns the parsed arguments.

    :returns: A dictionary with the parsed command line arguments.
    """

    # get common args
    parser_common = get_argparser(description='Convert ROS-Bags into faster readable data.')

    # add custom args
    parser_convert = argparse.ArgumentParser(parents=[parser_common])

    # data
    data = parser_convert.add_argument_group('data')
    data.add_argument("--sources", "-s", type=str, nargs='+',
                      help="List of paths to the ROS-Bag files or path to directory with ROS-Bags inside.")
    data.add_argument("--destination", "-d", type=str, help="The output directory.")

    # options
    options = parser_convert.add_argument_group('options')
    options.add_argument("--frame_skip", type=int, help="Number of frames to skip after reading a frame.")
    options.add_argument("--write_as_archive", type=bool,
                         help="Defines if the output should be written into a archive instead of plain files.")

    return parser_convert.parse_args()


if __name__ == '__main__':
    main()
