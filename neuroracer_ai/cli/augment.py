#!/usr/bin/env python

"""
This script augments the data specified by the sources.

TODO:
Most of the augmentation functions decode and encode the images, because they return an ImageSnapshot,
which holds the image as compressed image, but to edit the data you need the image uncompressed.
Maybe find a way with better performance.
"""

from __future__ import print_function

import argparse
import functools
import multiprocessing

from neuroracer_ai.utils.arg_handler import get_argparser
from neuroracer_ai.utils.bag_reader import get_container_name
from neuroracer_ai.utils.augment_functions import augment_rotation, augment_brightness, augment_flip, augment_blotches, augment_lines
from neuroracer_ai.utils.config_handler import NB_CPU
from neuroracer_ai.utils.config_handler import check_config, read_config_file
from neuroracer_ai.utils.data_handler import snapshot_generator_from_sources, write_snapshot_dict
from neuroracer_ai.utils.snapshot import ImageSnapshot


# For examples for augmentation see here: https://github.com/aleju/imgaug


def main():
    """
    Augments specified sources.
    """

    args = _get_arguments()

    config = {}

    if args.configfile:
        config = read_config_file(args.configfile)

    # override config file configurations with action line arguments
    if args.bag_sources:
        config["bag_sources"] = args.bag_sources
    elif "bag_sources" not in config:
        config["bag_sources"] = None

    if args.extracted_sources:
        config["extracted_sources"] = args.extracted_sources
    elif "extracted_sources" not in config:
        config["extracted_sources"] = None

    if args.frame_skip_bags:
        config["frame_skip_bags"] = args.frame_skip_bags
    elif "frame_skip_bags" not in config:
        config["frame_skip_bags"] = 0

    if args.frame_skip_extracted:
        config["frame_skip_extracted"] = args.frame_skip_extracted
    elif "frame_skip_extracted" not in config:
        config["frame_skip_extracted"] = 0

    if args.destination:
        config["destination"] = args.destination
    if args.camera_topic:
        config["camera_topic"] = args.camera_topic
    if args.action_topic:
        config["action_topic"] = args.action_topic
    if args.verbose:
        config["verbose"] = args.verbose
    if args.write_as_archive:
        config["write_as_archive"] = args.write_as_archive

    verbose = 1
    if "verbose" in config:
        verbose = config["verbose"]

    and_required_keys = ["destination", "compositions"]
    or_required_keys = [("bag_sources", "extracted_sources")]

    check_config(config, and_required_keys=and_required_keys, or_required_keys=or_required_keys)

    # read data from source
    snapshot_gen = snapshot_generator_from_sources(config["bag_sources"],
                                                   config["extracted_sources"],
                                                   camera_topic=config["camera_topic"],
                                                   action_topic=config["action_topic"],
                                                   frame_skip_bags=config["frame_skip_bags"],
                                                   frame_skip=config["frame_skip_extracted"],
                                                   return_dict=True)



    # A dictionary containing the augmented data.
    # Example:
    # The keys are composition names like "bag1_rotated"
    # The values are the augmented snapshot_lists for example of bag1, augmented with composition "rotated"

    for snapshot_dict in snapshot_gen:
        compositions = config["compositions"]
        pool = multiprocessing.Pool(processes=NB_CPU)
        _augment_snapshots_with_arguments = functools.partial(_augment_snapshots_with_compositions,
                                                              compositions=compositions)

        augmented_snapshots = pool.map(_augment_snapshots_with_arguments, snapshot_dict.items())[0]

        pool.close()
        pool.join()

        write_snapshot_dict(augmented_snapshots, destination=config["destination"], verbose=verbose)


def _augment_snapshots_with_compositions(item, compositions):
    """
    Creates a new snapshot list with the data in snapshot_list and augments it with the compositions.

    :param item: A tuple (source_name, snapshot_list)
    :type item: tuple
    :param compositions: A list of compositions to apply to the snapshot_list
    :type compositions: List[Dict]
    :return: A dictionary with the created container_names as keys and the responding snapshot_lists as values
    :rtype: dict[str, List[neuroracer_ai.utils.ImageSnapshot]]
    """
    source_name, snapshot_list = item
    augmented_snapshot_dict = {}

    for composition in compositions:
        container_name = get_container_name(composition["name"].format(source_name=source_name))

        augmented_snapshot_list = _augment_snapshots(snapshot_list,
                                                     composition)

        if container_name not in augmented_snapshot_dict:
            augmented_snapshot_dict[container_name] = []

        augmented_snapshot_dict[container_name].extend(augmented_snapshot_list)

    return augmented_snapshot_dict


def _build_augmentation_pipeline(composition):
    """
    Constructs a function (a pipeline) to augment the argument of this pipeline.

    :param composition: The composition defining the way of augmentation
    :type composition: dict
    :return: A function pipeline(snapshot) -> snapshot.
             The return value of this pipeline is a augmented snapshot.
    :rtype: Callable[[snapshot], snapshot]
    """

    aug_functions = []

    # rotation
    if "rotation" in composition:
        rotation = composition["rotation"]
        if "intensity" not in rotation:
            raise KeyError("You have to specify 'intensity' of rotation in composition '{}'"
                           .format(composition["name"]))

        rotation_func = functools.partial(augment_rotation, intensity=rotation["intensity"])
        aug_functions.append(rotation_func)

    # brightness
    if "brightness" in composition:
        brightness = composition["brightness"]
        if "intensity" not in brightness:
            raise KeyError("You have to specify 'intensity' of brightness in composition '{}'"
                           .format(composition["name"]))

        brightness_func = functools.partial(augment_brightness,
                                            intensity=brightness["intensity"])

        aug_functions.append(brightness_func)

    # lines
    if "lines" in composition:
        lines = composition["lines"]
        if "number" not in lines:
            raise KeyError("You have to specify 'number' of lines in composition '{}'".format(composition["name"]))

        lines_func = functools.partial(augment_lines,
                                       number=lines["number"])

        aug_functions.append(lines_func)

    # flip
    if "flip" in composition:
        if composition["flip"]:
            aug_functions.append(augment_flip)

    # blotches
    if "blotches" in composition:
        blotches = composition["blotches"]
        blotch_keys = ["number", "min_width", "max_width", "min_height", "max_height"]
        for k in blotch_keys:
            if k not in blotches:
                raise KeyError(
                    "You have to specify '{}' for blotches in composition '{}'".format(k, composition["name"]))

        blotches_func = functools.partial(augment_blotches,
                                          nb_blotches=blotches["number"],
                                          blotch_width=(blotches["min_width"], blotches["max_width"]),
                                          blotch_height=(blotches["min_height"], blotches["max_height"]))

        aug_functions.append(blotches_func)

    # build pipeline
    def pipeline(snapshot):
        image = snapshot.get_uncompressed_image()
        actions = snapshot.get_actions()
        for aug_function in aug_functions:
            image, actions = aug_function(image=image, actions=actions)
        return ImageSnapshot.copy(snapshot, uncompressed_image=image, actions=actions)

    return pipeline


def _augment_snapshots(snapshot_list, composition):
    """
    Augments the snapshot_list with the given composition.

    :param composition: The composition which defines the way of augmentation
    :type composition: dict
    :param snapshot_list: The list of snapshots to augment
    :type snapshot_list: List[snapshot]

    :return: A augmented list of snapshots
    :rtype: list[neuroracer_ai.utils.ImageSnapshot]
    """

    pipeline = _build_augmentation_pipeline(composition)
    augmented_snapshot_list = []

    if "count" not in composition:
        raise KeyError("You have to specify 'count' for each composition")
    count = composition["count"]

    for snapshot in snapshot_list:
        for _ in range(count):
            augmented_snapshot = pipeline(snapshot)
            augmented_snapshot_list.append(augmented_snapshot)

    return augmented_snapshot_list


def _get_arguments():
    """
    Creates the ArgumentParser and returns the parsed arguments.

    :returns: A dictionary with the parsed action line arguments.
    """

    # get common args
    parser_common = get_argparser(description='Augments data.')

    # add custom args
    parser_augment = argparse.ArgumentParser(parents=[parser_common])

    # data
    data = parser_augment.add_argument_group('data')
    data.add_argument("--bag_sources", type=str, nargs="+",
                      help="Path(s) to ROS-Bag file(s) or directory(ies) with ROS-Bag files to augment.")
    data.add_argument("--extracted_sources", type=str,
                      help="Path to a directory with extracted data to augment (created by convert.py)")
    data.add_argument("--destination", "-d", type=str, help="The output directory.")

    # options
    options = parser_augment.add_argument_group('options')
    options.add_argument("--frame_skip_bags", type=int,
                         help="Number of frames to skip after reading a frame from a bag.")
    options.add_argument("--frame_skip_extracted", type=int,
                         help="Number of frames to skip after reading a frame from extracted data.")
    options.add_argument("--write_as_archive", type=bool,
                         help="Defines if the output should be written into a archive instead of plain files.")

    return parser_augment.parse_args()


if __name__ == '__main__':
    main()
