#!/usr/bin/env python

import argparse
import os

import numpy as np
from ruamel.yaml import YAML

from neuroracer_ai.utils.data_handler import snapshot_generator_from_sources
from neuroracer_ai.utils import config_handler
from neuroracer_ai import (ImageProcessingParameters, Image2DProcessorSuite,
                           KerasArchitectures, AI, TrainParameters)
from neuroracer_ai.utils import history_logger
from neuroracer_ai.utils.arg_handler import get_argparser

MODEL_FILE_EXTENSION = ".hdf5"


def snapshot_list_to_data(snapshot_list):
    """
    Transforms an snapshot_list into a tuple of (x_data, y_data).

    x_data is a list of numpy.nd.array.
    len(x_data) will be the number of snapshots in the snapshot_list.
    Every x in x_data is a compressed image.

    y_data is a numpy.nd.array.
    y_data.shape[0] will be the number of snapshots in the snapshot_list.
    Every y in y_data is a numpy.nd.array containing the actions (steer, drive)


    :param snapshot_list: A list of ImageSnapshots
    :type snapshot_list: List[neuroracer_ai.utils.snapshot.ImageSnapshot]
    :param verbose: 0: do not print status information; 1: print status information
    :type verbose: int

    :returns: A tuple (x_data, y_data). x_data: List[np.nd.array], y_data: np.nd.array
    :rtype: tuple
    """

    nb_snapshots = len(snapshot_list)

    if nb_snapshots == 0:
        raise ValueError("Number of snapshots is zero.")

    # nb_actions = snapshot_list[0].get_nb_actions()
    # hack to get the system working with only steer as return value
    nb_actions = 1

    # create a list with np_snapshots values
    x_data = [None] * nb_snapshots

    y_data = np.empty((nb_snapshots, nb_actions), dtype=np.float32)

    for index, snapshot in enumerate(snapshot_list):
        x_data[index] = snapshot.get_image()
        y_data[index] = snapshot.get_actions_as_tupel()

    ###########################################################
    # This functions seems to be faster without multiprocessing.
    # Maybe try again with bigger datasets.
    #
    # x_data = shared.array((nb_snapshots, image_shape[0], image_shape[1],
    #                                   image_shape[2]), dtype=np.float32)
    # y_data = shared.array((nb_snapshots, nb_actions), dtype=np.float32)
    #
    # with Parallel(config_handler.NB_CPU) as p:
    #     for index, snapshot in p.iterate(enumerate(snapshot_list)):
    #         with p.lock:
    #             x_data[index] = snapshot.get_image()
    #             y_data[index] = snapshot.get_actions_as_tupel()
    ###########################################################

    return x_data, y_data


def update_if_not_none(config, extension):
    """
    Copies the values of extension into config, if extension[key] is not None.

    :param config: The config to update.
    :type config: dict
    :param extension: config will be updated with extensions content.
    :type extension: dict
    """

    for key, value in extension.items():
        if value is not None:
            config[key] = value


def get_architecture(architecture_str):
    """
    Translates a string defining the architecture into the enum.

    :param architecture_str: A string defining which architecture is used
    :type architecture_str: str
    :returns: An architecture building function
    :rtype: Callable
    """

    if not hasattr(KerasArchitectures, architecture_str):
        raise ValueError("Architecture '{}' is unknown".format(architecture_str))

    return getattr(KerasArchitectures, architecture_str)


# A list of all config keys available
config_keys = ["train_bag_sources", "train_extracted_sources", "evaluation_bag_sources",
               "evaluation_extracted_sources", "model_name", "model_dir", "mode", "num_epochs",
               "batch_size", "architecture", "camera_topic", "action_topic", "image", "verbose", "frame_skip_bags",
               "frame_skip_extracted", "runs", "log_file"]


def create_default_config_dict():
    """
    Returns a default config dict without processing commandline arguments nor config files.

    :returns: A default config dict
    :rtype: dict
    """

    config = {}

    for ck in config_keys:
        config[ck] = None

    default_values = {"mode": "smart",
                      "num_epochs": 10,
                      "batch_size": 120,
                      "architecture": "htw_modified_nvidia",
                      "verbose": 1,
                      "frame_skip_bags": 0,
                      "frame_skip_extracted": 0,
                      "log_file": ""}

    for key, value in default_values.items():
        config[key] = value

    return config


def key_in_dict(key, dictionary):
    """
    Returns True if key is in config and not None otherwise False

    :param key: The key to check.
    :type key: Any
    :param dictionary: A dictionary which couldt contain the key
    :type dictionary: dict
    :returns: True if key is in config and not None otherwise False
    :rtype: bool
    """
    return (key in dictionary) and (dictionary[key] is not None)


def check_config(config):
    """
    Checks if the config dictionary is valid

    :param config: The configuration dictionary to check
    :type config: Dict[str, Any]
    :raises KeyError: If a needed key is not specified
    """

    # check train sources
    if not key_in_dict("train_bag_sources", config):
        if not key_in_dict("train_extracted_sources", config):
            raise KeyError("You have to specify '--train_bag_sources' or"
                           "'--train_extracted_sources'. Type 'train.py --help' for more information.")

    if not key_in_dict("evaluation_bag_sources", config):
        if not key_in_dict("evaluation_extracted_sources", config):
            raise KeyError("You have to specify '--evaluation_bag_sources'"
                           "or '--evaluation_extracted_sources'."
                           "Type 'train.py --help' for more information.")

    needed_keys = ["verbose", "camera_topic", "action_topic", "frame_skip_bags", "frame_skip_extracted", "runs"]

    # check needed keys
    for needed_key in needed_keys:
        if not key_in_dict(needed_key, config):
            raise KeyError("You have to specify '--{}'."
                           "Type 'train.py --help' for more information.".format(needed_key))


def check_config_extension(extension):
    """
    Raises an KeyError if extension contains an unknown configkey.

    :param extension: A dictionary
    :type extension: dict

    :raises KeyError: If extension contains an unknown configkey
    """

    for key in extension:
        if key not in config_keys:
            raise KeyError("Unknown key found: '{}'".format(key))


def process_args(args):
    """
    Processes the arguments provided by _get_arguments().
    Returns a dictionary with the items in args but nested in the same
    way like the data in the configfile.

    :param args: args returned by _get_arguments()
    :returns: A dictionary with the same values like args, but nested in the same
              way like the configfile
    :rtype: dict
    """

    # get the arguments as dict
    args = args.__dict__

    yaml_keys = ["image"]

    args_dict = {}

    yaml = YAML()
    for key, value in args.items():
        if key in yaml_keys and value is not None:
            value = yaml.load(value)
        args_dict[key] = value

    return args_dict


def generate_config_dict():
    """
    Creates a config dict by processing default configurations,
    configfile parameters and commandline arguments.

    :raises KeyError: If configfile contains unknown keys
    """

    # default config
    config = create_default_config_dict()

    # get arguments
    args = _get_arguments()

    # configfile
    if args.configfile:
        configfile_extension = config_handler.read_config_file(args.configfile)
        check_config_extension(configfile_extension)
        config.update(configfile_extension)

    # command line arguments
    args_dict = process_args(args)
    update_if_not_none(config, args_dict)

    check_config(config)

    return config


def get_image_shape(config):
    """
    Calculates the image shape for the ai depending on the config.

    :param config: The configuration dictionary
    :type config: dict

    :returns: The image shape of the ai
    :rtype: tuple
    """

    resize = config["image"]["resize"]
    return resize["height"], resize["width"], resize["depth"]


def get_image_processing_parameters(config):
    """
    Creates the image_processing_parameters for the ai

    :param config: The configuration dictionary
    :type config: dict

    :returns: An ImageProcessingParameter object, if needed otherwise None
    :rtype: ImageProcessingParameter
    """

    if "image" in config and config["image"]:
        image = config["image"]
    else:
        return None

    # define resize parameters
    resize_parameters = None

    if "resize" in image and image["resize"]:
        resize = image["resize"]
        resize_parameters = ImageProcessingParameters.ResizeParameters(
            x_scaling=resize["width"],
            y_scaling=resize["height"],
            relative_scaling=resize["relative"])

    # define opencv_color_space_conv_func
    opencv_color_space_conv_func = None

    if "opencv_color_space_conv_func" in image:
        opencv_color_space_conv_func = image["opencv_color_space_conv_func"]

    # define flip option
    flip_option = None
    if "flip_option" in image:
        flip_option = ImageProcessingParameters.Flip(image["flip_option"])

    # define crop parameters
    crop_parameters = None

    if "crop" in image and image["crop"]:
        crop = image["crop"]
        crop_parameters = ImageProcessingParameters.CropParameters(
            top=crop["top"], bottom=crop["bottom"], left=crop["left"],
            right=crop["right"], relative=crop["relative"])

    # define normalize
    normalize = False
    if "normalize" in image:
        normalize = image["normalize"]

    # define decode_image
    decode_image = True
    if "decode_image" in image:
        decode_image = image["decode_image"]

    # build image_processing_parameters
    image_processing_parameters = ImageProcessingParameters(
        resize_parameters=resize_parameters,
        opencv_color_space_conv_func=opencv_color_space_conv_func,
        flip_option=flip_option,
        crop_parameters=crop_parameters,
        normalize=normalize,
        decode_image=decode_image)

    return image_processing_parameters


def get_train_parameters(model_dir, train_bag_sources, train_extracted_sources,
                         evaluation_bag_sources, evaluation_extracted_sources, camera_topic,
                         action_topic, verbose, frame_skip_bags, frame_skip_extracted):

    train_snapshot_generator = snapshot_generator_from_sources(train_bag_sources,
                                                               train_extracted_sources,
                                                               camera_topic=camera_topic,
                                                               action_topic=action_topic,
                                                               frame_skip_bags=frame_skip_bags,
                                                               frame_skip=frame_skip_extracted)

    evaluation_snapshot_generator = snapshot_generator_from_sources(evaluation_bag_sources,
                                                                    evaluation_extracted_sources,
                                                                    camera_topic=camera_topic,
                                                                    action_topic=action_topic,
                                                                    frame_skip_bags=frame_skip_bags,
                                                                    frame_skip=frame_skip_extracted)

    if verbose > 0:
        # transform into train/evaluation data
        print("Transform snapshot list into train data.")

    train_generator = (snapshot_list_to_data(train_batch)
                       for train_batch in train_snapshot_generator)

    eval_generator = (snapshot_list_to_data(eval_batch)
                      for eval_batch in evaluation_snapshot_generator)

    return TrainParameters(train_data_generator=train_generator,
                           validation_data_generator=eval_generator,
                           checkpoint_dir=model_dir)


def main():
    """
    Reads sources from disk and starts to train an AI.

    :raises ValueError: If the mode-argument is not in a valid state.
    """

    # configuration
    config = generate_config_dict()

    verbose = config["verbose"]

    # get data
    train_parameters = get_train_parameters(model_dir=config["model_dir"],
                                            train_bag_sources=config["train_bag_sources"],
                                            train_extracted_sources=config["train_extracted_sources"],
                                            evaluation_bag_sources=config["evaluation_bag_sources"],
                                            evaluation_extracted_sources=config["evaluation_extracted_sources"],
                                            camera_topic=config["camera_topic"],
                                            action_topic=config["action_topic"],
                                            verbose=verbose,
                                            frame_skip_bags=config["frame_skip_bags"],
                                            frame_skip_extracted=config["frame_skip_extracted"])

    runs = config["runs"]
    model_dir = config["model_dir"]

    for run in runs:
        # load or create ai
        model_name = run["model_name"]
        model_file = os.path.join(model_dir, model_name + MODEL_FILE_EXTENSION)
        mode = run["mode"]

        if os.path.isfile(model_file):
            if mode == "smart":
                mode = "load"
            elif mode == "create":
                raise IOError("File '{}' already exists."
                              "If you want to keep training this model use '--mode load'"
                              "or '--mode smart' instead. Or '--mode overwrite'"
                              "to overwrite this model.".format(model_file))
        else:
            if mode == "smart":
                mode = "overwrite"
            elif mode == "load":
                raise IOError("File '{}' not found. If you want to create a new model"
                              "use another mode instead.".format(model_file))

        architecture = None

        # load ai
        if mode == "load":
            ai = AI.load(name=model_name, model_dir=model_dir)
            if verbose > 0:
                print("loading ai {}".format(os.path.join(model_dir, model_name)))
        # create ai
        elif mode == "create" or mode == "overwrite":
            if verbose > 0:
                print("creating new ai at {}".format(os.path.join(model_dir, model_name)))
            # define architecture
            architecture = get_architecture(run["architecture"])

            # define image_shape
            image_shape = get_image_shape(run)

            # define image processing parameters
            image_processing_parameters = get_image_processing_parameters(run)

            # define processor_suite
            processor_suite = Image2DProcessorSuite(image_processor_params=image_processing_parameters)

            ai = AI.create(
                name=run["model_name"],
                model_dir=config["model_dir"],
                architecture=architecture,
                architecture_params={"image_shape": image_shape},
                processor_suite=processor_suite)

        else:
            raise ValueError("couldn't load ai. mode={}".format(mode))

        if ("batch_size" in run) and (run["batch_size"] is not None):
            train_parameters.batch_size = run["batch_size"]

        if ("num_epochs" in run) and (run["num_epochs"] is not None):
            train_parameters.epochs = run["num_epochs"]

        # train the ai
        history = ai.train(train_parameters, verbose=verbose)

        history_logger.log_history(history,
                                   architecture_func=architecture,
                                   model_name=model_name,
                                   destination=config["log_file"])


def _get_arguments():
    """
    Creates the ArgumentParser and returns the parsed arguments.

    :returns: A dictionary with the parsed action line arguments.
    """

    # get common args
    parser_common = get_argparser(description='Reads a rosbag and trains a model with it.')

    # add custom args
    parser_train = argparse.ArgumentParser(parents=[parser_common])

    # data
    data = parser_train.add_argument_group('data')
    # train
    data.add_argument("--train_bag_sources", type=str, nargs="+",
                      help="Path(s) to ROS-Bag file(s) or directory(ies) with ROS-Bag files for training")
    data.add_argument("--train_extracted_sources", type=str,
                      help="Path to a directory with extracted data for training(created by convert.py)")
    # evaluation
    data.add_argument("--evaluation_bag_sources", type=str, nargs="+",
                      help="Path(s) to ROS-Bag file(s) or directory(ies) with ROS-Bag files for evaluation")
    data.add_argument("--evaluation_extracted_sources", type=str,
                      help="Path to a directory with extracted data for evaluation (created by convert.py)")

    # model
    model = parser_train.add_argument_group('model')
    model.add_argument("--model_dir", "-d", type=str,
                       help="The path to the directory for the model data. A new model will be created here")
    model.add_argument("--frame_skip_bags", type=int,
                       help="Number of frames to skip before reading the next frame")
    model.add_argument("--frame_skip_extracted", type=int,
                       help="Number of frames to skip before reading the next frame")

    # image processing parameters
    processing = parser_train.add_argument_group('processing')
    processing.add_argument("--image", type=str,
                            help="Defines the way the input images are processed. See README for more details.")

    return parser_train.parse_args()


if __name__ == '__main__':
    main()
