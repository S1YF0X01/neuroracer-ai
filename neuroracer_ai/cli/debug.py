#!/usr/bin/env python

from __future__ import print_function
import math
import sys
import copy
import os
import argparse
from abc import ABCMeta, abstractmethod

import cv2
import numpy as np
from ruamel.yaml import YAML
from ruamel.yaml.scanner import ScannerError

from neuroracer_ai import AI

CV_LOAD_AS_IS = -1
MISSING_TASK_FIELD_ERROR = "Configured task is missing a {0} field! Task without {0}: {1}"


# todo: switching this to snapshots would result in a unified file api

def convert_strings_to_paths(raw_paths):
    """
    Goes through a list of string paths expands the user dir and collects all
    files on the first level. If a entry of raw_paths is a file is simply gets
    added to the list. If it is a dir it will get all files in this dir but
    will not check subdirs.

    :param raw_paths: paths to convert
    :type raw_paths: list[str]
    :return: converted and expanded paths
    :rtype: list[str]
    """

    if raw_paths is None:
        return []

    paths = []

    for raw_path in raw_paths:
        expanded_path = os.path.expanduser(raw_path)
        if os.path.isfile(expanded_path):
            paths.append(expanded_path)
        else:
            dir_content = os.listdir(expanded_path)
            for dir_item in dir_content:
                dir_item_path = os.path.join(expanded_path, dir_item)
                if os.path.isfile(dir_item_path):
                    paths.append(dir_item_path)

    return paths


def remap_numbers(value, old_min, old_max, new_min, new_max):
    """
    Small helper function which maps a value from one number range to a another
    one.

    :param value: value which gets remapped
    :type value: float
    :param old_min: min boundary of the old range
    :type old_min: float
    :param old_max: max boundary of the old range
    :type old_max: float
    :param new_min: min boundary of the new range
    :type new_min: float
    :param new_max: max boundary of the new range
    :type new_max: float
    :return: remapped value
    :rtype: float
    """

    old_span = old_max - old_min
    new_span = new_max - new_min
    return (value - old_min) * new_span / old_span + new_min


class AbstractTask:
    """
    Abstract base class for all debug tasks.
    """

    __metaclass__ = ABCMeta

    def __init__(self, name=""):
        self._name = name

    @abstractmethod
    def do_task(self, ai):
        """
        Executes the configured task with on the given ai.

        :param ai: ai on which the task gets executed
        :type ai: neuroracer_ai.AI
        """

        pass

    @staticmethod
    def from_dict(task_dict):
        """
        Abstract factory method to create a task instance based on a dict. If
        creation failed a tuple of type (NoneType, List) will be returned. The
        returned list contains the messages with caused the failing.

        :param task_dict: data dict
        :type task_dict: dict[str, Any]
        :return: constructed AbstractTask instance if it succeeded otherwise None and a list with errormessages
        :rtype: Tuple[AbstractTask, Optional[List]]
        """

        task_class_mapping = {
            "visualize_saliency": VisualizeSaliencyTask,
            "visualize_activation": VisualizeActivationTask,
            "print_summary": PrintSummaryTask,
            "print_weights": PrintWeightsTask,
            "print_prediction": PrintPredictionTask
        }

        task_class = None
        task_type = task_dict["type"]
        if task_type in task_class_mapping:
            task_class = task_class_mapping[task_dict["type"]]

        if task_class is not None:
            params = copy.deepcopy(task_dict)
            del params["type"]

            error_messages = task_class._check_init_params(params)
            if len(error_messages) > 0:
                return None, error_messages

            return task_class(**params), error_messages

    @staticmethod
    @abstractmethod
    def _check_init_params(params):
        """
        Checks if the given params are ok for the class. If something is
        missing or wrong a list with the error messages gets returned.

        :param params: possible init params
        :type params: Dict[str, Any]
        :return: a list of error messages if errors have been found otherwise a empty list
        :rtype: List[str]
        """

        return []


class AbstractVisualizationTask(AbstractTask):
    """
    Abstract base class for all visualization related tasks.
    """

    __metaclass__ = ABCMeta

    def __init__(self, output_dir, name="", layer_name=None, layer_idx=None, filter_indices=None, image_paths=None):
        super(AbstractVisualizationTask, self).__init__(name)
        self._layer_name = layer_name
        self._layer_idx = layer_idx
        self._filter_indices = filter_indices
        self._seed_input_paths = convert_strings_to_paths(image_paths)
        self._output_dir = output_dir

    def do_task(self, ai):
        for path in self._seed_input_paths:
            img = cv2.imread(os.path.expanduser(path), CV_LOAD_AS_IS)
            _, img = cv2.imencode(".jpg", img)
            img = np.expand_dims(img, axis=0)
            self._visualize(ai, path, img)
        else:
            self._visualize(ai)

    def _visualize(self, ai, seed_path=None, seed_image=None):
        visualization = self._visualize_on_ai(ai, seed_image)

        file_prefix = "visualization"
        if self._name:
            file_prefix = self._name

        filename = "{}.jpg".format(file_prefix)
        if seed_path is not None:
            filename = "{}_{}.jpg".format(file_prefix, os.path.basename(seed_path))

        filepath = os.path.join(os.path.expanduser(self._output_dir), filename)
        cv2.imwrite(filename=filepath, img=visualization)

    @abstractmethod
    def _visualize_on_ai(self, ai, seed_input):
        """
        Calls the right visualization function on the ai instance.

        :param ai: ai instance
        :type ai: neuroracer_ai.AI
        :param seed_input: seed to be used for the visualization
        :type seed_input: np.ndarray
        :return: visualization
        :rtype: np.ndarray
        """
        pass

    @staticmethod
    def _check_init_params(params):
        errors = []

        if "output_dir" not in params:
            errors.append("output_dir field is missing!")

        if "layer_idx" not in params and "layer_name" not in params:
            errors.append("Either layer_idx or layer_name must be defined!")

        return errors


class VisualizeSaliencyTask(AbstractVisualizationTask):

    def _visualize_on_ai(self, ai, seed_input):
        return ai.visualize_saliency(
            layer_name=self._layer_name,
            layer_idx=self._layer_idx,
            filter_indices=self._filter_indices,
            seed_input=seed_input)


class VisualizeActivationTask(AbstractVisualizationTask):

    def _visualize_on_ai(self, ai, seed_input):
        return ai.visualize_activation(
            layer_name=self._layer_name,
            layer_idx=self._layer_idx,
            filter_indices=self._filter_indices,
            seed_input=seed_input)


class PrintSummaryTask(AbstractTask):

    def do_task(self, ai):
        print(ai.get_summary())

    @staticmethod
    def _check_init_params(params):
        return AbstractTask._check_init_params(params)


class PrintWeightsTask(AbstractTask):

    def __init__(self, name="", layer_name=None, layer_idx=None):
        super(PrintWeightsTask, self).__init__(name=name)
        self._layer_name = layer_name
        self._layer_idx = layer_idx

    def do_task(self, ai):
        weights = ai.get_weights(layer_name=self._layer_name, layer_idx=self._layer_idx)
        print(weights)

    @staticmethod
    def _check_init_params(params):
        errors = []

        if "layer_idx" not in params and "layer_name" not in params:
            errors.append("Either layer_idx or layer_name must be defined!")

        return errors


class PrintPredictionTask(AbstractTask):

    def __init__(self, image_paths, draw_image=True, steer_tuple_pos=0,
                 drive_tuple_pos=1, steer_min=-1, steer_max=1, drive_min=0,
                 drive_max=1, name="", output_dir=None):

        super(PrintPredictionTask, self).__init__(name=name)
        self._image_paths = convert_strings_to_paths(image_paths)
        self._draw_image = draw_image
        self._steer_tuple_pos = steer_tuple_pos
        self._drive_tuple_pos = drive_tuple_pos
        self._steer_min = steer_min
        self._steer_max = steer_max
        self._drive_min = drive_min
        self._drive_max = drive_max
        self._output_dir = output_dir

    def do_task(self, ai):
        for path in self._image_paths:
            image_decoded = cv2.imread(os.path.expanduser(path), CV_LOAD_AS_IS)
            _, image = cv2.imencode(".jpg", image_decoded)
            prediction = ai.predict(np.expand_dims(image, axis=0))
            # steer = prediction[0][self._steer_tuple_pos]
            # drive = prediction[0][self._drive_tuple_pos]
            # hack to get the system working with only steer as return value
            steer = prediction[0]
            drive = 0
            if not self._draw_image:
                print("prediction for {}: steer {}, drive {}".format(path, steer, drive))
            else:
                image_with_arrows = self._draw_arrows_on_img(image_decoded, drive, steer)
                output_path = os.path.join(self._output_dir, os.path.basename(path))
                cv2.imwrite(output_path, image_with_arrows)

    def _draw_arrows_on_img(self, image, drive, steer):
        image_width = image.shape[1]
        image_height = image.shape[0]
        image_hor_center = int(image_width / 2)
        arrow_origin = (image_hor_center, image_height)

        drive_in_px = int(remap_numbers(drive, self._drive_min, self._drive_max, 0, image_height))
        drive_arrow_end = (image_hor_center, drive_in_px)
        new_image = cv2.arrowedLine(image, arrow_origin, drive_arrow_end, (0, 255, 0), 4)

        arrow_length = image_height / 2
        steer_angle = remap_numbers(steer, self._steer_min, self._steer_max, -45, 45)
        steer_angle_rad = math.radians((steer_angle))
        steer_arrow_end = (
            image_hor_center + int(arrow_length * math.sin(steer_angle_rad)),
            image_height - int(arrow_length * math.cos(steer_angle_rad)))

        new_image = cv2.arrowedLine(new_image, arrow_origin, steer_arrow_end, (255, 0, 0), 4)

        return new_image

    @staticmethod
    def _check_init_params(params):
        errors = []

        if "image_paths" not in params:
            errors.append("image_paths must be defined!")

        if "output_dir" not in params \
                and ("draw_image" not in params
                     or "draw_image" in params and params["draw_image"]):
            errors.append("output_dir must be defined if draw_images is not set to False!")

        return errors


def fix_config_dict_keys(config_dict):
    """
    This function fixes the raw dict read from yaml to be compatible with the
    __init__ params of the Config class.

    :param config_dict: dict to reformat
    :type config_dict: dict[str, Any]
    :return: fixed dict
    :rtype: Dict[str, Any]
    """

    fixed_config_dict = {}

    for key, value in config_dict.items():
        fixed_key = key.lower().strip().replace("-", "_")
        fixed_config_dict[fixed_key] = value

    return fixed_config_dict


def transform_task_dicts(task_dicts):
    """
    Transforms the dict to there respective AbstractTask implementations. If
    the type is unknown it is ignored.

    :param task_dicts: task dict read from yaml
    :return: list of AbstractTask instances
    :rtype: Tuple[list[AbstractTask], Optional[Dict[str, List[str]]]
    """

    tasks = []
    errors = {}

    for task_dict in task_dicts:
        if "type" in task_dict:

            task, error_messages = AbstractTask.from_dict(task_dict)
            if task is not None:
                tasks.append(task)
            else:
                key = "Creating a {} task failed! The following errors have been found:".format(
                    task_dict["type"])
                if "name" in task_dict:
                    key = "Creating the {} ({}) task failed! The following errors have been found:".format(
                        task_dict["name"], task_dict["type"])

                errors[key] = error_messages

    return tasks, errors


def main():
    """
    Main functions which handles the parsing of the config file and bails out
    on errors.

    :return: 0 if successful otherwise -1
    :rtype: int
    """

    args = _get_arguments()

    config_path = os.path.expanduser(args.configfile)
    if not os.path.isfile(config_path):
        print("The given configfile ({}) doesn't exist!".format(config_path))
        return -1

    with open(os.path.expanduser(args.configfile)) as f:
        try:
            yaml_parser = YAML()
            config_dict = yaml_parser.load(f)
        except ScannerError as e:
            print("Parsing the config file failed with the following error: {}".format(e))
            return -1

        if not isinstance(config_dict, dict):
            print("The structure of the config is wrong! You can look up the structure in the example config.")
            return -1

        if "model_dir" not in config_dict:
            print("Config is missing the \"model_dir\" field!")
            return -1

        model_dir = os.path.expanduser(config_dict["model_dir"])
        if not os.path.exists(model_dir):
            print("The dir defined by \"model_dir\" ({}) doesn't exist!".format(model_dir))
            return -1

        if "model_name" not in config_dict:
            print("Config is missing the \"model_name\" field!")
            return -1

        model_name = config_dict["model_name"]
        model_path = os.path.join(model_dir, model_name + ".nrai")
        if not os.path.exists(model_path):
            print("The file defined by \"model_name\" ({}) doesn't exist!".format(model_name))
            return -1

        config_dict = fix_config_dict_keys(config_dict)
        tasks, errors = transform_task_dicts(config_dict["tasks"])

        if len(errors) > 0:
            print("The tasks config contains the following errors:")
            print("")
            for error, messages in errors.items():
                print(" - " + error)
                for message in messages:
                    print("   - " + message)
                print("")
            return -1

    ai = AI.load(name=model_name, model_dir=model_dir)

    for task in tasks:
        task.do_task(ai)

    return 0


def _get_arguments():
    """
    Creates the ArgumentParser for this cli tool.

    :return: created ArgumentParser
    :rtype: argparse.ArgumentParser
    """

    parser = argparse.ArgumentParser(description="Extracs debug information from a nrai model.")
    parser.add_argument("configfile")

    return parser.parse_args()


if __name__ == "__main__":
    sys.exit(main())
