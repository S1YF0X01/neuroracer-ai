# Train

This module is used to train an [AI](./ai.py) instance.

## Usage

#### Help

To print the help:

```bash
nrai-train -h
# or
nrai-train --help
 ```
 
#### Configfile

You can specify a yaml configuration file.

```bash
nrai-train --configfile example_configs/train.yaml
```

This file could look like this:

```yaml
train_bag_sources: [../training_data/bags/]
evaluation_bag_sources: [../training_data/bags/]
model_dir: ../trained_data/
model_name: example
mode: smart
verbose: 1
num_epochs: 5
batch_size: 120
architecture: htw_modified_nvidia
image:
    resize:
        width: 192
        height: 108
        relative: False
    flip: null
    crop: null
    normalize: False
    opencv_color_space_conv_func: null

```

You can specify a command line argument for every entry in this configfile.
Commandline arguments override the configfile values.

### Train Sources

#### Train Bag Sources

You can specify a list of rosbags.
All rosbags are loaded and used for the training of the model.

```bash
nrai-train --train_bag_sources res/source1.bag res/source2.bag # [...]
```

You can also specify a directory (or multiple directories).
This will search recursive for "\*.bag" files in these directories.

```bash
nrai-train --train_bag_sources res/ # [...]
```

#### Train Extracted Sources

You can specify a main directory of extracted data created by [nrai-convert](cli/convert.py).
The data in this directory will be used to train the model.
A main-directory should have the following structure:

```
main_directory/
+-- res1/
|   +-- res1.yaml
|   +-- res1_<timestamp1>.jpg
|   +-- res1_<timestamp2>.jpg
|   +-- [...]
+-- res2/
    +-- res2.yaml
    +-- res2_<timestamp1>.jpg
    +-- res2_<timestamp2>.jpg
    +-- [...]
```

### Evaluation Sources

#### Evaluation Bag Sources

You can specify a list of rosbags with `--evaluation_bag_sources`
(In the same way as `--train_bag_sources`)
All rosbags are loaded and used for the evaluation of the model.

#### Evaluation Extracted Sources

With `--evaluation_extracted_sources` you can specify a main directory of
extracted data created by [nrai-convert](cli/convert.py)
(In the same way as `--train_extracted_sources`).
The data in this directory will be used to evaluate the model.

#### Model Dir

You can specify a directory where the data of the neural network will be stored.

```bash
nrai-train --model_dir model/
```

#### Model Name

You can specify the name of the model.

```bash
nrai-train --model_name myModelName
```

#### Architecture

If a new model is created you can specify the architecture of this model.

```bash
nrai-train --architecture htw_modified_nvidia
```

The following architectures are available:
- htw\_modified\_nvidia
- nvidia
- lillicrap

This option will be ignored, if the model gets loaded.

#### Mode

You can set one of the following modes:

- create
- load
- overwrite
- smart

```bash
nrai-train --mode [create|load|overwrite|smart]
```

##### Create

Tries to create a new model. If a model with the same name already exists it fails.

##### Load

Tries to load a existing model. If the model doesn't exists it fails.

##### Overwrite

Creates a new model. If a model with the same name already exists then it is overwritten.

##### Smart

If the model already exists it loads it, otherwise it creates a new model with the specified name.

#### Camera/Command Topic

There has to be a *camera topic* and a *command topic* in the rosbag.
These topics can be specified by the following options:

```bash
nrai-train --camera_topic "/camera/front/image_rect_color/compressed"
# and
nrai-train --command_topic "/neuroracer_teleop_joy/drive_cmd_vector"
```

This option will be ignored, if the model is loaded.

#### Number of Epochs

Defines how many epochs to train.

```bash
nrai-train --num_epochs 5
```

#### Batch Size

Defines the used Batch Size.

```bash
nrai-train --batch_size 120
```

#### Image Processing

To preprocess the images for the model you can provide some parameters to train.
Be aware that under certain conditions it is
necessary that the input images from the sources have all the same size.

The argument is a dictionary in inline yaml format:

```bash
nrai-train --image "{resize: {width: 20, height: 30, relative: False}, flip: null, crop: null, normalize: False, opencv_color_space_conv_func: null}"
```

##### Resize

Defines how the images are resized:

`{resize: {width: 20, height: 30, relative: False}}`

means: resize all input images to (20x30) pixels.

`{resize: {width: 0.5, height: 0.5, relative: True}}`

means: resize all input images to 50% of the original size.

Be aware that when you specify `relative: True` all input images must have the same size.

##### Flip

Defines how images are flipped:

- `{flip: null}`: Do not flip
- `{flip: 0}`: Flip vertical
- `{flip: 1}`: Flip horizontal
- `{flip: -1}`: Flip vertical and horizontal

##### Crop

Defines how images are cropped:

- `{crop: null}`:

  do not crop.

- `{crop: {top: 4, bottom: 3, left: 2, right: 1, relative: False}}`:
  
  crop 4 pixels on the top edge, 3 bottom, 2 left and right 1 pixel.
  
- `{crop: {top: 0.2, bottom: 0.2, left: 0.2, right: 0.2, relative: True}}`:

  crop 20% on each side.

## Example

Given the configfile "example\_configs/train.yaml":

```yaml
train_bag_sources: [training_data/bags/test/]
evaluation_bag_sources: [training_data/bags/train/]
model_dir: model/
model_name: example
mode: smart
verbose: 1
num_epochs: 5
batch_size: 120
architecture: htw_modified_nvidia
image:
    resize:
        width: 192
        height: 108
        relative: False
    flip: null
    crop: null
    normalize: False
    opencv_color_space_conv_func: null
```

And rosbags in "training\_data/bags/train/" and "training\_data/bags/test/" you could do:

```bash
nrai-train --configfile example_configs/train.yaml
```

This will load the rosbags and creates 2 files "model/example.hdf5" and "model/example.nrai" to save the model data.
