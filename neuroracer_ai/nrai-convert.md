# Converter

This module is used to convert rosbags into faster readable data.
To understand the format of this data
see the [example](#example).

## Usage

### Help

To print the help:

```bash
nrai-convert -h
# or
nrai-convert --help
 ```
 
### Configfile

You can specify a yaml configuration file.

```bash
nrai-convert --configfile config/default.yaml
```

This file should look like this:

```yaml
sources: ["res/test.bag"]
destination: "out/"
camera_topic: "/camera/front/image_rect_color/compressed"
command_topic: "/neuroracer_teleop_joy/drive_cmd_vector"
```

Commandline arguments override the configfile values.

### Sources

You can specify a list of rosbags.
All rosbags are loaded and converted.

```bash
nrai-convert --sources res/source1.bag res/source2.bag # [...]
```

You can also specify a directory (or multiple directories).
This will search recursive for "\*.bag" files in these directories.

```bash
nrai-convert --sources res/ # [...]
```
### Destination

You can specify *one* directory where the extracted data will be stored.

```bash
nrai-convert --destination out/
```

### Camera/Command Topic

There has to be a *camera topic* and a *command topic* in the rosbag.
These topics can be specified by the following options:

```bash
nrai-convert --camera_topic "/camera/front/image_rect_color/compressed"
# and
nrai-convert --command_topic "/neuroracer_teleop_joy/drive_cmd_vector"
```

## Example

Given the rosbag "res/example.bag" you could do:

```bash
nrai-convert --source res/example.bag --destination out/
```

The following output will be produced:

```
out/
+-- example/
    +-- example.yaml
    +-- example_{timestamp1}.jpg
    +-- example_{timestamp2}.jpg
    +-- example_{timestamp3}.jpg
    ...
```

The timestamps will be big integer numbers.

The example.yaml file will look like this:

```yaml
test_{timestamp1}: {drive: 1.0, steer: 0.0}
test_{timestamp2}: {drive: 0.9, steer: 0.1}
test_{timestamp3}: {drive: 0.8, steer: 0.2}
```

Each line in the example.yaml has one jpg file with the same timestamp.
